import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducers from '../Reducers'; //Import the reducer
import rootSaga from '../Sagas'; //Import the saga
import playerMiddleware from '../middleware/player'
// import streamMiddleware from '../middleware/stream'
import downloadMiddleware from '../middleware/downloader'
// Connect Saga middleware to the Redux store
const sagaMiddleware = createSagaMiddleware();
let initialState = {};

export default function configureStore(initialState) {
    //const sagaMiddleware = createSagaMiddleware();
    
    const store = createStore(
        reducers,
        initialState,
        applyMiddleware(sagaMiddleware,playerMiddleware)
    );
    sagaMiddleware.run(rootSaga);
    return store;
}
