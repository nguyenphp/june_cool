import React from 'react'
import { View, Modal, ActivityIndicator } from 'react-native'

const ModalLoading = (props) => {
    return (
        <Modal
            visible={props.visible}
            transparent={true}
        >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={
                    {
                        backgroundColor: 'white', width: 80, height: 80, borderRadius: 5, justifyContent: 'center', alignItems: 'center' ,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,

                        elevation: 5,
                    }
                }>
                    <ActivityIndicator size="large" color="#bdc3c7" />
                </View>
            </View>
        </Modal>
    )
}
export default ModalLoading;