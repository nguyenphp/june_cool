import React, { Component } from 'react';
import {
  View,
  Text,
  StatusBar,
} from 'react-native';
import Header from './Header';
import AlbumArt from './AlbumArt';
import TrackDetails from './TrackDetails';
import SeekBar from './SeekBar';
import Controls from './Controls';
// import Video from 'react-native-video';
import  Video  from 'react-native-video';
export default class PlayerMusic extends Component {
  constructor(props) {
    super(props);
    this.audioElement = React.createRef();
    this.state = {
      paused: true,
      totalLength: 1,
      currentPosition: 0,
      selectedTrack: 0,
      repeatOn: false,
      shuffleOn: false,
    };
  }
  async componentDidMount() {
    // const soundObject = new Audio.Sound();
    // try {
    //   await soundObject.loadAsync(require('../../../resource/chuyenngaysauke.mp3'));
    //   await soundObject.playAsync();
    //   // Your sound is playing!
    //   console.log("sournd objec", await soundObject.getStatusAsync())
    //   // Don't forget to unload the sound from memory
    //   // when you are done using the Sound object
    //   await soundObject.unloadAsync();
    // } catch (error) {
    //   // An error occurred!
    // }
  }
  setDuration(data) {

   console.log("Status here",data);
   this.setState({totalLength: Math.floor(data.duration)});
  }

  setTime(data) {
    // let m = a.positionMillis/a.durationMillis * 100;
    // let work=a.positionMillis/1000;
    // let full=a.durationMillis/1000;
    // let fulltime= Math.round(full);
    // let time = Math.round(work);
    // let ab= Math.round(m);
    this.setState({currentPosition: Math.floor(data.currentTime)});
  }

  seek(time) {
    time = Math.round(time);
    this.refs.audioElement && this.refs.audioElement.seek(time);
    this.setState({
      currentPosition: time,
      paused: false,
    });
  }

  onBack() {
    if (this.state.currentPosition < 10 && this.state.selectedTrack > 0) {
      this.refs.audioElement && this.refs.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(() => this.setState({
        currentPosition: 0,
        paused: false,
        totalLength: 1,
        isChanging: false,
        selectedTrack: this.state.selectedTrack - 1,
      }), 0);
    } else {
      this.refs.audioElement.seek(0);
      this.setState({
        currentPosition: 0,
      });
    }
  }

  onForward() {
    if (this.state.selectedTrack < this.props.tracks.length - 1) {
      this.refs.audioElement && this.refs.audioElement.seek(0);
      this.setState({ isChanging: true });
      setTimeout(() => this.setState({
        currentPosition: 0,
        totalLength: 1,
        paused: false,
        isChanging: false,
        selectedTrack: this.state.selectedTrack + 1,
      }), 0);
    }
  }
  loadStart(data) {
    console.log("data", data)
  }


  render() {
    const track = this.props.tracks[this.state.selectedTrack];
    const video = this.state.isChanging ? null : (
     
      <Video source={require('../../../resource/chuyenngaysauke.mp3')} // Can be a URL or a local file.
        ref="audioElement"
        paused={this.state.paused}               // Pauses playback entirely.
        resizeMode="cover"           // Fill the whole screen at aspect ratio.
        repeat={true}                // Repeat forever.
        onLoadStart={this.loadStart} // Callback when video starts to load
        onLoad={this.setDuration.bind(this)}    // Callback when video loads
        onProgress={this.setTime.bind(this)}    // Callback every ~250ms with currentTime
        onEnd={this.onEnd}           // Callback when playback finishes
        onError={this.videoError}    // Callback when video cannot be loaded
        style={styles.audioElement}
          rate={1.0}
        volume={1.0}
        isMuted={false}
        />
      // <Video
      // ref={r=>this.audioElement = r}
      //   paused={!this.state.paused}
      //   source={require('../../../resource/chuyenngaysauke.mp3')}
      //   rate={1.0}
      //   volume={1.0}
      //   isMuted={false}
      //   resizeMode="cover"
      //   shouldPlay={!this.state.paused}
      //   isLooping
      //   onLoadStart={this.loadStart.bind(this)} // Callback when video starts to load
      //   onLoad={this.setDuration.bind(this)}    // Callback when video loads
      //   onPlaybackStatusUpdate={(e)=>this.setTime(e)}    // Callback every ~250ms with currentTime
      //   onEnd={this.onEnd}           // Callback when playback finishes
      //   onError={this.videoError}    // Callback when video cannot be loaded
      //   style={styles.audioElement}
      // />
    );

    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Header onDownPress={this.props.onDownPress} message="Playing From Charts" />
        <View style={{height:'50%'}}>
        <AlbumArt url={track.albumArtUrl} />
        </View>

     
        <TrackDetails title={track.title} artist={track.artist} />
        <SeekBar
          onSeek={this.seek.bind(this)}
          trackLength={this.state.totalLength}
          onSlidingStart={() => this.setState({ paused: true })}
          currentPosition={this.state.currentPosition} />
        <Controls
          onPressRepeat={() => this.setState({ repeatOn: !this.state.repeatOn })}
          repeatOn={this.state.repeatOn}
          shuffleOn={this.state.shuffleOn}
          forwardDisabled={this.state.selectedTrack === this.props.tracks.length - 1}
          onPressShuffle={() => this.setState({ shuffleOn: !this.state.shuffleOn })}
          onPressPlay={() => this.setState({ paused: false })}
          onPressPause={() => this.setState({ paused: true })}
          onBack={this.onBack.bind(this)}
          onForward={this.onForward.bind(this)}
          paused={this.state.paused} />

          {video}
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'rgb(4,4,4)',
  },
  audioElement: {
    height: 10,
    width: 0,
    backgroundColor: 'red',

  }
};
