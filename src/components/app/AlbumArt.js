import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Swiper from 'react-native-swiper'
import { ScrollView } from 'react-native-gesture-handler';
const AlbumArt = ({
  url,
  onPress,
  song
}) => (
    <Swiper style={styles.wrapper} activeDotColor="#7d9356" dotColor="rgba(200, 214, 229,0.4)"
      loop={false}
      index={0}
    >
      <View style={styles.container}>

        <Image
          style={styles.image}
          source={{ uri: url }}
        />

      </View>
      <ScrollView style={[styles.container]} contentContainerStyle={{alignItems:'center'}}>
   
        <Text style={styles.titleLyric}>Lời bài hát</Text>
        <Text style={[styles.titleLyric,{fontSize:15,marginTop:15}]}>{song.lyric}</Text>
  
      </ScrollView>

    </Swiper>

  );

export default AlbumArt;

const { width, height } = Dimensions.get('window');
const imageSize = width - 48;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 24,
    paddingRight: 24,
  },
  image: {
    width: imageSize,
    height: imageSize,
  },
  wrapper: {},
  titleLyric:{ color: 'white',fontSize:25,fontFamily:'SVN-HandOfSeanPro',fontWeight:'bold' }
})
