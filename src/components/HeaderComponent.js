/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
const HeaderComponent = (props) => {
    const navigation = useNavigation();
    return (
        <>

            <SafeAreaView style={{ backgroundColor: 'white' }}>
                <View style={styles.header}>

                    <View style={styles.leftHeader}>

                        <TouchableOpacity onPress={() => navigation.goBack()} style={{}}>
                            <Icon name="close-outline" style={{ fontWeight: '100' }} size={30} />
                        </TouchableOpacity>

                    </View>
                    <View style={styles.middleHeader}>
                        <Text style={styles.title}>{props.title}</Text>
                    </View>
                    <View style={styles.rightHeader}>
                        <TouchableOpacity onPress={()=>props.onPress()} style={{ justifyContent: 'center', alignItems: 'center', width:40}}>
                            <Text style={{ fontSize: 14, fontWeight: '500', color: '#3498db' }}>Đăng</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 50,
        backgroundColor: 'white',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderRadius: 2,
        borderColor: '#ddd',
        // shadowColor: '#707070',
        // shadowOffset: { width: 0, height: 0.5 },
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 1,
        // marginBottom: 5,
    },
    leftHeader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',


    },
    middleHeader: {
        flex: 5,
        justifyContent: 'center',
        alignItems: 'center',

    },
    rightHeader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: '#888888',
        fontSize: 15
    }

});

export default React.memo(HeaderComponent);
