export const TOKEN = 'ASYNC_TOKEN_V3';
export const PROFILE = 'ASYNC_PROFILE';
export const FIRST_TIME_LOAD = 'FIRST_TIME_LOAD';
export const LANGUAGE = "LANGUAGE";
export const USER_ID = "USER_ID"
export const DEVICE_TOKEN = 'DEVICE_TOKEN';