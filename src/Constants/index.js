import Actions from './Actions';
import Persist from './Persist';

export default {
    Actions,
    Persist,
}