import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import API from '../Services/API';
import { pause, pointSong, deleteSong, nextSong, lastSong } from '../Actions/song'
import MusicControl from 'react-native-music-control';
const MusicItem = (props) => {
    const [listMusic, setListMusic] = useState([])
    const { item } = props;
    const setUpPlayer = (song) => {
        MusicControl.setNowPlaying({
            title: song.title,
            artwork: song.cover_image, // URL or RN's image require()
            artist: song.artist,
            //album: 'Thriller',
            //genre: 'Post-disco, Rhythm and Blues, Funk, Dance-pop',
            duration: props.durationTime, // (Seconds)
          })
        MusicControl.enableBackgroundMode(true);
        MusicControl.enableControl('play', true)
        MusicControl.enableControl('pause', true)
        MusicControl.enableControl('stop', false)
        MusicControl.enableControl('nextTrack', true)
        MusicControl.enableControl('previousTrack', true)

      
      
    }
    const playSong = (song) => {
        setUpPlayer(song);
        if (props.currentSong == song) {
            props.onPlay()
        } else {
            props.onPoint(song);
        }

    }
    return (
        <TouchableOpacity onPress={() => playSong(item)} style={{ width: '100%', height: 60, flexDirection: 'row', backgroundColor: 'white', alignItems: 'center', borderBottomWidth: 0.8, borderColor: '#ecf0f1' }}>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Image style={{ width: 40, height: 40, borderRadius: 40 }} source={{ uri: item.cover_image }} />
            </View>
            <View style={{ flex: 4, alignItems: 'flex-start' }}>
                <Text style={{ color: 'black', fontFamily: 'SVN-HandOfSeanPro', fontSize: 15 }}>{props.title}</Text>
                <Text style={{ color: 'black', fontSize: 10, fontWeight: '200' }}>{item.artist}</Text>
            </View>
            <TouchableOpacity onPress={() => playSong(item)} style={{ flex: 1, alignItems: 'center' }}>
                <Image style={{ width: 20, height: 20 }} source={props.currentSong == item && props.isPlaying ? require('../../resource/pause-button.png') : require('../../resource/play-button.png')} />
            </TouchableOpacity>


        </TouchableOpacity>
    )
}
const mapStateToProps = (state, props) => {
    return {
        playList: state.songs.playList,
        currentSong: state.songs.currentSong,
        isPlaying: state.songs.isPlaying,
        durationTime: state.songs.durationTime,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPoint: (song) => {
            dispatch(pointSong((song)))
        },
        onDelete: (index) => {
            dispatch(deleteSong(index))
        },
        onPlay: () => {
            dispatch(pause())
        },
        onNext: () => {
            dispatch(nextSong())
        },
        onLast: () => {
            dispatch(lastSong())
        },
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MusicItem)
