import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, Image, Button, ActionSheetIOS, TouchableOpacity, TextInput, ScrollView, Alert, DatePickerIOS, Platform } from 'react-native'
import ImageView from 'react-native-image-view';
import HeaderComponent from '../components/HeaderComponent';
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-crop-picker';
import { showMessage, hideMessage } from "react-native-flash-message";
import Icon from 'react-native-vector-icons/Ionicons';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import { Modalize } from 'react-native-modalize';
import moment from 'moment'
import Axios from 'axios';
const AddStoryScreen = (props) => {
    const [visible, setVisible] = React.useState(false)
    const { item } = props;
    const [date, setDate] = useState(new Date(1598051730000));
    const [formatDate, setFormatDate] = useState("")
    const [mode, setMode] = useState('datetime');
    const [show, setShow] = useState(false);
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [selectImage, setSelectImage] = useState(null);
    const navigation = useNavigation();
    const modalizeRef = useRef(null);

    const onOpen = () => {
        modalizeRef.current?.open();
    };
    console.log("item", item)
    const postStory = () => {
        if (selectImage == null || title == "" || content == "") {
            Alert.alert("Thông báo đeiii", "Nhập hết mới được bấm đăng")
        } else {
            var body = new FormData();
            body.append("title", title)
            body.append("content", content)
            let uri = Platform.OS=='ios'?selectImage.path.replace("file://", "/private"):selectImage.path
            let tmpImage = {
            //    file: selectImage,
                uri: selectImage.path,
                type: selectImage.mime,
                name: selectImage.filename,
            }
            body.append("image", tmpImage);
            body.append("date_time", moment(date).format("YYYY-MM-DDTHH:MM:SS"));
            uploadStory(body)
            //navigation.goBack()
        }

    }
    const uploadStory = async (formData) => {
        console.log("form data here", formData)
        // setVisibleModal(true)
        let config = {
            headers: {
                // 'Content-Type': 'multipart/form-data',
                'Content-Type': 'application/json',
            },
        };
        await Axios.post(
            "https://nongsanogreen.com/nhstory/public/api/stories",
            formData,
            config,
        ).then((res) => {
            console.log("upload image success", res)
            if (res.status === 201) {
                //setVisibleModal(false)
                //Alert.alert("Upload ảnh thành công")
                showMessage({
                    message: "Upload Story thành công",
                    description: "Câu chuyện vừa thêm 1 tập mới ^^!",
                    type: "success",
                    duration: 3000
                });

                navigation.replace("BottomTab")
                //navigation.state.params.getData({ selected: true });

            }
        }).catch((err) =>
            console.log("error", err)
        )
    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const onPress = () => {
        console.log("consolasdasd")
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: ["Huỷ bỏ", "Chọn từ thư viện", "Chụp ảnh"],
                //destructiveButtonIndex: 0,
                cancelButtonIndex: 0
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    // cancel action
                } else if (buttonIndex === 1) {
                    ImagePicker.openPicker({
                        //multiple: true
                        cropping: true,
                        width: 1920,
                        height: 1080,
                        compressImageQuality:0.5
                    }).then(image => {
                        console.log(image);
                        setSelectImage(image)

                    });
                } else if (buttonIndex === 2) {
                    ImagePicker.openCamera({
                        cropping: true,
                        width: 1920,
                        height: 1080,
                        compressImageQuality:0.5
                    }).then(image => {
                        setSelectImage(image)
                    });
                }
            }
        );
    }
    const selectDateTime = () => {
        let tmpTime = moment(date).format("hh:mm DD-MM-YYYY")
        setFormatDate(tmpTime);
        modalizeRef.current?.close();
    }
    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <HeaderComponent onPress={() => postStory()} title="Thêm Story" />
            <KeyboardAwareScrollView style={{ width: '100%', height: '100%' }}>
                <ScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={{ alignItems: 'center' }}>

                    <View style={styles.uploadPhoto}>
                        {!selectImage ? <TouchableOpacity onPress={() => onPress()} style={{ width: 500, height: 200, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../resource/photo.png')} style={{ width: 60, height: 60 }} />
                            <Text style={{ fontSize: 12, color: '#bdc3c7' }}>Chọn Ảnh/Chụp Ảnh</Text>
                        </TouchableOpacity> : <Image style={{ width: '100%', height: 200, borderRadius: 7 }} resizeMode="cover" source={{ uri: selectImage.path }} />

                        }
                        {selectImage && <TouchableOpacity onPress={() => setSelectImage(null)} style={{ position: 'absolute', bottom: 10, right: 10, zIndex: 99, width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="trash-outline" color={"white"} size={18} />
                        </TouchableOpacity>}
                    </View>

                    <View style={styles.titleContainer}>
                        <TextInput
                            style={{ width: '100%', height: '100%', marginLeft: 10 }}
                            value={title}
                            onChangeText={(text) => setTitle(text)}
                            placeholder="Nhập tiêu đề"
                        />
                    </View>
                    <View style={styles.contentContainer}>
                        <TextInput
                            style={{ width: '95%', height: '100%', marginLeft: 10, marginTop: 5 }}
                            multiline={true}
                            numberOfLines={8}
                            value={content}
                            onChangeText={(text) => setContent(text)}
                            placeholder="Nhập nội dung"
                        />
                    </View>

                    <View style={styles.titleContainer}>
                        <TouchableOpacity onPress={onOpen} style={{ width: '100%', height: '100%', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 14, color: '#bdc3c7', marginLeft: 10 }}>{formatDate ? formatDate : "Chọn ngày"}</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => postStory()} style={styles.button}>
                        <Text style={{ fontSize: 15, color: 'white', fontWeight: '500' }}>Đăng Story</Text>
                    </TouchableOpacity>

                    <View>
                        <Button onPress={() => showDatepicker()} title="Show date picker!" />
                    </View>
                    <View>
                        <Button onPress={() => showTimepicker()} title="Show time picker!" />
                    </View>

                </ScrollView>
            </KeyboardAwareScrollView>
            <Modalize adjustToContentHeight={true} ref={modalizeRef}>
                <TouchableOpacity onPress={() => selectDateTime()}><Text>Chọn</Text></TouchableOpacity>
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={"datetime"}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                    style={{ width: "100%", backgroundColor: "white" }}
                />
            </Modalize>
            {/* {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={"datetime"}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                    style={{width: "100%", backgroundColor: "white"}}
                />
            )} */}
            {/* {show && <DatePickerIOS
        date={date}
        onDateChange={onChange}
      />} */}
        </View>
    )
}
const styles = StyleSheet.create({
    uploadPhoto: {
        width: '95%',
        height: 200,
        borderWidth: 0.5,
        borderStyle: 'dashed',
        borderRadius: 7,
        borderColor: '#95a5a6',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    titleContainer: {
        width: '95%',
        height: 50,
        borderWidth: 0.5,
        //borderStyle: 'dotted',
        borderRadius: 7,
        borderColor: '#95a5a6',
        justifyContent: 'center',
        marginTop: 20
    },
    contentContainer: {
        width: '95%',
        height: 250,
        borderWidth: 0.5,
        //borderStyle: 'dotted',
        borderRadius: 7,
        borderColor: '#95a5a6',
        justifyContent: 'center',
        marginTop: 20
    },
    button: {
        width: '95%',
        height: 50,
        //borderWidth: 0.5,
        //borderStyle: 'dotted',
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        backgroundColor: '#2ecc71'
    }

})
export default AddStoryScreen;