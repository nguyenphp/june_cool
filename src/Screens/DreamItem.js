import React from 'react';
import { View, Text,Image,TouchableOpacity } from 'react-native'
import ImageView from 'react-native-image-view';

// import FastImage from 'react-native-fast-image';
const images = [
    {
        source: {
            uri: 'https://cdn.pixabay.com/photo/2017/08/17/10/47/paris-2650808_960_720.jpg',
        },
        title: 'Paris',
        width: 806,
        height: 720,
    },
];
const DreamItem = (props) => {
    const [visible,setVisible] = React.useState(false)
    const {item} = props;
    console.log("item", item)
    return (
        <TouchableOpacity onPress={props.openImage}>
            <View style={{flex:1,height:150,margin:2.5}}>
            {/* <ImageView
                images={[
                    {source: item.image,
                    title: 'Paris',
                    width: 806,
                    height: 720,}
                ]}
                imageIndex={props}
                isVisible={visible}
                onClose={()=>setVisible(false)}
                renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
            /> */}
            <Image source={{uri:item.photo_url}} style={{width:"100%",height:150}}/>
            </View>
        </TouchableOpacity>
    )
}
export default DreamItem;