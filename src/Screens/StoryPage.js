import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, Text, View, SafeAreaView, Dimensions, Animated, TouchableOpacity, Image } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
const labels = ["Cart", "Delivery Address", "Order Summary", "Payment Method", "Track"];
import Carousel from 'react-native-snap-carousel';
import API from '../Services/API';
import FastImage from 'react-native-fast-image';
import { BlurView } from "@react-native-community/blur";
import { Modalize } from 'react-native-modalize';
import ModalLoading from '../components/ModalLoading';
import moment from 'moment';
import Axios from 'axios';
import Credentials from '../Services/Credentials';
import { useNavigation } from '@react-navigation/native';
export const DEVICE_HEIGHT = Math.round(Dimensions.get('window').height)
export const DEVICE_WIDTH = Math.round(Dimensions.get('window').width)
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013'
}

const StoryPage = () => {
  const [currentPosition, setCurrentPosition] = React.useState(0)
  const [listStory, setListStory] = useState([])
  const carouselRef = useRef(null);
  const [currentImage, setCurrentImage] = useState("");
  const [currentItem, setCurrentItem] = useState({})
  const modalizeRef = useRef(null);
  const [visible, setVisible] = useState(false)
  const [handle, setHandle] = useState(false);
  const [avatar, setAvatar] = useState("https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png")
  const navigation = useNavigation();
  const handlePosition = position => {
    setHandle(position === 'top');
  };
  const onOpen = () => {
    modalizeRef.current?.open();
  };
  const onPageChange = (position) => {
    setCurrentPosition(position)
  }
  const setCurrentIndexImage = (index) => {
   // console.log("item here", index)
    //setCurrentImage(listStory[index].image)
    setCurrentItem(listStory[index])
  }
  const getData = () => {
    API.getStory().then((res) => {
      //console.log("res", res)
      setListStory(res.data)
      setCurrentItem(res.data[0])
      setVisible(false)
    })
  }
  useEffect(() => {
    setVisible(true)
    getData()
    API.getAvatar().then((res) => {
      setAvatar(res.data.avatar)
    })
    // API.login()


  }, [])
  const renderDate = (date) => {

    let tmpDate = moment(date);
    let day = tmpDate.day();
    let formatDate = tmpDate.format("DD-MM-YYYY");

    let dayString = ""
    switch (day) {
      case 2: dayString = "Thứ hai"
        break;
      case 3: dayString = "Thứ ba"
        break;
      case 4: dayString = `Thứ "bốn"`
        break;
      case 5: dayString = "Thứ năm"
        break;
      case 6: dayString = "Thứ sáu"
        break;
      case 7: dayString = "Thứ bảy"
        break;
      case 8: dayString = "Chủ nhật"
        break;

      default:
        break;
    }
    return dayString + ", " + formatDate
  }
  const renderItem = ({ item, index }) => {

    return (
      <TouchableOpacity onPress={() => onOpen()} style={styles.slide}>

        <FastImage style={styles.image} resizeMode="cover" source={{ uri: item.image, priority: FastImage.priority.low, }}>
          <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0 }}>
            <Text style={[styles.titleStyle, { fontSize: 30, color: 'white', margin: 0, marginLeft: 20 }]}>{item.title}</Text>
            <Text style={styles.dateStyle}>{renderDate(item.dateTime)}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[styles.descriptionStyle1, { flexWrap: 'wrap', flexShrink: 1, fontSize: 12, color: 'white', marginLeft: 20 }]} numberOfLines={3}>{item.content}</Text>
            </View>
          </View>
        </FastImage>

      </TouchableOpacity>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ModalLoading visible={visible} />
      <TouchableOpacity
        onPress={() => navigation.navigate("AddStoryScreen", { getData: getData() })}
        style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 20, right: 20, zIndex: 99 }}>
        <Image source={require('../../resource/plus.png')} style={{ width: 25, height: 25 }} />
      </TouchableOpacity>
      <View style={styles.header}>


        <Image style={{ width: 80, height: 80, borderRadius: 40, marginTop: 10 }} source={{ uri: avatar }} />


        <Text style={styles.headerText}>The Story Of Us</Text>

      </View>
      <Modalize ref={modalizeRef}
        modalStyle={{ marginTop: 30 }}
      //panGestureAnimatedValue={animated}
      //snapPoint={HEADER_HEIGHT}
      //withHandle={handle}
      //handlePosition="inside"
      //handleStyle={{ top: 13, width: 40, height: handle ? 6 : 0, backgroundColor: '#bcc0c1' }}
      >
        <SafeAreaView style={[styles.slide, { width: '100%' }]}>

          <FastImage style={styles.image1} resizeMode="cover" source={{ uri: currentItem.image, priority: FastImage.priority.low, }} >
          <TouchableOpacity
            onPress={() => navigation.navigate("EditStoryScreen", { currentItem: currentItem })}
            style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 20, right: 20, zIndex: 99 }}>
            <Image source={require('../../resource/pencil.png')} style={{ width: 25, height: 25 }} />
          </TouchableOpacity>
          </FastImage>
          <Text style={styles.titleStyle1}>{currentItem.title}</Text>
          <Text style={styles.dateStyle1}>{renderDate(currentItem.dateTime)}</Text>
          <Text style={styles.descriptionStyle}>{currentItem.content}</Text>
        </SafeAreaView>
      </Modalize>
      <FastImage style={styles.imageBackground} source={{ uri: currentItem.image, priority: FastImage.priority.low, }}>
        <BlurView
          style={styles.absolute}
          blurType="light"
          blurAmount={10}
          reducedTransparencyFallbackColor="white"
        />
      </FastImage>
      <View style={{ height: DEVICE_HEIGHT / 1.6, position: 'absolute', bottom: 0, }}>
        {/* <View>
          <Text style={{ color:'white', marinLeft: 20 }}>{currentItem.dateTime}</Text>
        </View> */}
        {listStory && <Carousel

          ref={carouselRef}
          sliderWidth={DEVICE_WIDTH}
          sliderHeight={DEVICE_HEIGHT / 3}
          itemWidth={DEVICE_WIDTH - 60}
          data={listStory}
          renderItem={renderItem}
          onSnapToItem={(index) => setCurrentIndexImage(index)}
          slideStyle={{ height: 500 }}
          style={{ backgroundColor: 'red' }}
        //hasParallaxImages={true}
        />}
      </View>


    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  scrollView: {
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 7,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0

  },
  image1: {
    width: '100%',
    height: 200,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10

  },
  priceStyle: {
    position: 'absolute', bottom: 0, left: 0,
    backgroundColor: '#464444'
  },
  blurViewPrice: {
    position: 'absolute', bottom: 0, left: 0,
  },
  textPriceStyle: {
    color: 'white',
    margin: 5
  },
  paginationStyle: { position: 'absolute', bottom: 0 },
  paginationText: {
    color: 'white'
  },
  slide: {
    //borderWidth: 1,
    //flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    height: DEVICE_HEIGHT / 1.7,
    borderRadius: 10,
    width: DEVICE_WIDTH - 60
  },
  buttonTitle: {
    height: 50,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
    minWidth: 70
    // flexWrap:'wrap',
    // flexDirection:'row'
  },
  titleStyle: {
    color: 'black',
    fontSize: 30,
    marginTop: 10,
    margin: 20,
    fontFamily: 'SVN-HandOfSeanPro',
    textShadowColor: '#585858',
    textShadowOffset: { width: 5, height: 5 },
    textShadowRadius: 10,

  },
  titleStyle1: {
    color: 'black',
    fontSize: 30,
    marginTop: 10,
    margin: 20,
    fontFamily: 'SVN-HandOfSeanPro',


  },
  descriptionStyle: {
    color: 'black',
    fontSize: 18,
    marginTop: 10,
    //margin: 30,
    //fontFamily: 'SVN-HandOfSeanPro',
    margin: 10
  },
  descriptionStyle1: {
    color: 'black',
    fontSize: 18,
    marginTop: 10,
    //margin: 30,
    //fontFamily: 'SVN-HandOfSeanPro',
    margin: 10,
    textShadowColor: '#585858',
    textShadowOffset: { width: 5, height: 5 },
    textShadowRadius: 10,
  },
  imageBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    opacity: 0.6
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  header: {
    width: '100%',
    //flexDirection: 'row',
    //height: 250,
    position: 'absolute',
    zIndex: 9,
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerText: {
    fontSize: 40,
    fontFamily: 'SVN-BrownhillScript',
    //fontWeight: 'bold',
    //marginLeft: 10,
    height: 80,
    margin: 10
  },
  dateStyle: {
    color: 'white', fontSize: 8, marginLeft: 20, fontWeight: 'bold',
    textShadowColor: '#585858',
    textShadowOffset: { width: 5, height: 5 },
    textShadowRadius: 10,
  },
  dateStyle1: {
    color: 'black', fontSize: 10, fontWeight: 'bold',
    textShadowColor: 'black',
    textShadowOffset: { width: 5, height: 5 },
    textShadowRadius: 10,
  }
})
export default StoryPage