import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import API from '../Services/API';
import MusicItem from './MusicItem'
import ModalLoading from '../components/ModalLoading';
import { connect } from 'react-redux'
import { pause,pointSong,deleteSong,addSongs } from '../Actions/song'
const MusicPage = (props) => {
    const [listMusic,setListMusic] = useState([])
    const [visible,setVisible] = useState(false)
    useEffect(()=>{
        setVisible(true)
        API.getListMusic().then((res)=>{
            //console.log("res",res)
            setListMusic(res.data)
            props.addSongs(res.data)
            setVisible(false)
            //setCurrentItem(res[0])
        })
    },[])
    return (
        <View style={{width:'100%',height:'100%'}}>
            <ModalLoading visible={visible}/>
            <FlatList
            data={listMusic}
            keyExtractor={(item)=>"key"+item.id}
            renderItem={({item})=>
                <MusicItem item={item} title={item.title}/>
            }
            />
        </View>
    )
}
const mapStateToProps = (state,props) => {
    return {
        playList: state.songs.playList,
        currentSong: state.songs.currentSong,
        isPlaying: state.songs.isPlaying,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPoint: (song) => {
            dispatch(pointSong((song)))
        },
        onDelete: (index) => {
            dispatch(deleteSong(index))
        },
        onPlay: () => {
            dispatch(pause())
        },
        addSongs: (songs) => {
            dispatch(addSongs((songs)))
        },
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MusicPage)
