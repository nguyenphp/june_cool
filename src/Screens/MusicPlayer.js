import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import { connect } from 'react-redux'
import API from '../Services/API';
import { pause,pointSong,deleteSong, nextSong, lastSong, switchMode,seekProgress, progress } from '../Actions/song'
import Header from '../components/app/Header';
import AlbumArt from '../components/app/AlbumArt';
import TrackDetails from '../components/app/TrackDetails';
import SeekBar from '../components/app/SeekBar';
import Controls from '../components/app/Controls';
import { useNavigation } from '@react-navigation/native';
import date from '../../util/date'
const MusicPlayer = (props) => {
    const [listMusic,setListMusic] = useState([])
    const [repeatOn,setRepeatOn] = useState(false)
    const {item} = props;
    const playSong = (song) => {
        props.onPoint(song);
    }
    //const track = this.props.tracks[this.state.selectedTrack];
    const navigation = useNavigation()
    const song = props.currentSong
    const getCurrentValue = () => {
        if (props.progressTime && props.totalTime) {
            return parseFloat(props.progressTime) / props.totalTime;
        }
        return 0;
    }
    const clickRepeat = () => {
       
        
    }
    console.log("asdasdasdsa",props.playMode )
    // console.log("asdasdasdsa",props.progressTime )
    return (
      <SafeAreaView style={styles.container}>
       
        <Header onDownPress={()=>navigation.goBack()} message="Playing From Charts" />
        <View style={{height:'50%'}}>
        <AlbumArt url={song.song_cover} song={song} />
        </View>

     
        <TrackDetails title={song.title} artist={song.artist} />
        <SeekBar
          onSeek={(value)=>props.onChange(value)}
          trackLength={props.durationTime}
          //onValueChange={(value)=>props.onValueChange(value)}
          //onSlidingStart={() => this.setState({ paused: true })}
          currentPosition={Math.floor(props.progressTime)} />
        <Controls
        playMode={props.playMode}
           onPressRepeat={() => props.onSwitchMode()}
           repeatOn={repeatOn}
        //   shuffleOn={this.state.shuffleOn}
          forwardDisabled={props.currentSong === props.playList[props.playList.length-1]}
        //   onPressShuffle={() => this.setState({ shuffleOn: !this.state.shuffleOn })}
          onPressPlay={() => props.onPlay()}
          onPressPause={() => props.onPlay()}
          onBack={()=>props.onLast()}
          onForward={()=>props.onNext()}
          paused={!props.isPlaying} />


      </SafeAreaView>
    )
}
const styles = {
    container: {
      flex: 1,
      backgroundColor: 'rgb(4,4,4)',
    },
    audioElement: {
      height: 10,
      width: 0,
      backgroundColor: 'red',
  
    }
  };
const mapStateToProps = (state,props) => {
    return {
        playList: state.songs.playList,
        currentSong: state.songs.currentSong,
        progressTime: state.songs.progressTime,
        durationTime: state.songs.durationTime,
        isPlaying: state.songs.isPlaying,
        playMode: state.songs.playMode,
       
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPoint: (song) => {
            dispatch(pointSong((song)))
        },
        onDelete: (index) => {
            dispatch(deleteSong(index))
        },
        onPlay: () => {
            dispatch(pause())
        },
        onNext: () => {
            dispatch(nextSong())
        },
        onLast: () => {
            dispatch(lastSong())
        },
        onSwitchMode: () => {
            dispatch(switchMode())
        },
        onChange: (value) => {
            dispatch(seekProgress(value))
        },
        onValueChange: (value) => {
            dispatch(progress(value))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MusicPlayer)
