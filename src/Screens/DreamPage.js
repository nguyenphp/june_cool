import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, Image, Animated, ActivityIndicator, StatusBar, ActionSheetIOS, TouchableOpacity, Alert, Platform } from 'react-native';
import DreamItem from './DreamItem';
import ImageView from 'react-native-image-view';
import axios from 'axios'
import FastImage from 'react-native-fast-image';
import ModalLoading from '../components/ModalLoading';
import API from '../Services/API';
import { Icon } from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import { showMessage, hideMessage } from "react-native-flash-message";
import { debounce } from 'lodash';
const data = [
    {
        id: 1,
        source: { uri: "https://scontent.fsgn5-4.fna.fbcdn.net/v/t1.0-9/118582353_4619347104757282_8555082620960185716_n.jpg?_nc_cat=1&_nc_sid=8bfeb9&_nc_ohc=1bYAnYp_JgQAX83wlcy&_nc_ht=scontent.fsgn5-4.fna&oh=723d7b9dbe2823cf7395fbae69a5a76f&oe=5F6F80E4" },
        title: 'June 1',

    },
    {
        id: 2,
        source: require('../../resource/album/june2.jpg'),
        title: 'June 1',

    },
    {
        id: 3,
        source: require('../../resource/album/june3.jpg'),
        title: 'June 1',

    },
    {
        id: 4,
        source: require('../../resource/album/june4.jpg'),
        title: 'June 1',

    }
]
const DreamPage = () => {
    const [data, setData] = React.useState([])
    const [visible, setVisible] = React.useState(false)
    const [index, setIndex] = React.useState(0)
    const [scrollY, setScrollY] = useState(new Animated.Value(0))
    const [visibleModal, setVisibleModal] = useState(false)
    const [listPhoto, setListPhoto] = useState([])
    const [page, setPage] = useState(1)
    const [lastPage, setLastPage] = useState(0)
    const [isLoadMore, setIsLoadMore] = useState(false)
    const [cover, setCover] = useState("")
    React.useEffect(() => {
        setVisibleModal(true)
        getData(1)


    }, [])
    const getData = (page) => {
        API.getListPhoto(page).then((res) => {
            console.log("response here", res.data)
            setLastPage(res.meta.last_page)
            if (page == 1) {
                setListPhoto(res.data)
            } else if (page > 1) {
                setListPhoto(listPhoto.concat(res.data))
            }
            let listImageView = []
            res.data.map((item)=>{
                let imageItem = {
                    source: {uri: item.photo_url}
                }
                listImageView.push(imageItem);
            })
            setData(listImageView);
            setVisibleModal(false)
            setIsLoadMore(false)
        })
        API.getAvatar().then((res) => {
            setCover(res.data.cover)
        })
    }
    const loadMore = () => {
        if (isLoadMore == false) {
            if (page <= lastPage + 1) {

                getData(page + 1),
                    setPage(page + 1),
                    setIsLoadMore(true)

            }
        }
    }
    const openImage = (index) => {
        setVisible(true)
        setIndex(index)
    }
    const renderFooter = () => {
        return (

            <View>
                {isLoadMore && <ActivityIndicator size="small" />}
            </View>


        )
    }
    const headerHeight = scrollY.interpolate({
        inputRange: [50, 200],
        outputRange: [200, 50],
        extrapolate: 'clamp',
    });
    const viewOpacity = scrollY.interpolate({
        inputRange: [0, 200 / 2, 200],
        outputRange: [1, 1, 0],
        extrapolate: 'clamp',
    });
    const textOpacity = scrollY.interpolate({
        inputRange: [0, 200 / 2, 200],
        outputRange: [0, 0.3, 1],
        extrapolate: 'clamp',
    });
    const onPress = () => {
        console.log("consolasdasd")
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: ["Huỷ bỏ", "Chọn từ thư viện", "Chụp ảnh"],
                //destructiveButtonIndex: 0,
                cancelButtonIndex: 0
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    // cancel action
                } else if (buttonIndex === 1) {
                    ImagePicker.openPicker({
                        //multiple: true
                    }).then(images => {
                        console.log(images);
                        var body = new FormData();
                        let listImage = []
                        // images.map((item)=>{
                        let image = {
                            file:images,
                            uri: images.path,
                            type: images.mime,
                            name: images.filename,
                        }
                             listImage.push(image)
                        // })
                        //console.log("image here", images)
                        body.append('image_files[]', listImage);

                        uploadImage(body)

                    });
                } else if (buttonIndex === 2) {
                    ImagePicker.openCamera({

                    }).then(image => {
                        console.log(image);
                        var body = new FormData();
                        let listImage = []

                        let imageSelect = {
                            file: image,
                            uri: image.path,
                            type: image.mime,
                            name: image.filename,
                        }
                        listImage.push(imageSelect)

                        body.append('image_files[]', listImage);
                        uploadImage(body)
                    });
                }
            }
        );
    }
    const uploadImage = async (formData) => {
        console.log("form data here", formData)
        setVisibleModal(true)
        let config = {
            headers: {
                'Content-Type': 'application/json',

            },
        };
        await axios.post(
            "https://nongsanogreen.com/nhstory/public/api/photos/",
            formData,
            config,
        ).then((res) => {
            console.log("upload image success", res)
            if (res.status === 200) {
                setVisibleModal(false)
                //Alert.alert("Upload ảnh thành công")
                showMessage({
                    message: "Upload ảnh thành công",
                    description: "Đã thêm 1 ảnh vào kho ảnh của Chủn và Cún",
                    type: "success",
                    duration: 3000
                });

            }
        }).catch((err) =>
            console.log("error", err)
        )
    }
    return (

        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <StatusBar backgroundColor="white" />
            <ModalLoading visible={visibleModal} />
            {/* <Image style={{width:'100%',height:200}} resizeMode="cover" source={require('../../resource/libraryBanner.png')}/> */}
            <Animated.View style={{ width: '100%', height: headerHeight, alignItems: 'center' }}>
                <Animated.Image style={{ width: '100%', height: headerHeight, opacity: viewOpacity }} resizeMode="cover" source={{ uri: cover }} />
                <Animated.Text style={{ fontSize: 15, fontWeight: 'bold', position: 'absolute', top: 15, opacity: textOpacity }}>Thư viện ảnh</Animated.Text>
            </Animated.View>
            <TouchableOpacity onPress={() => onPress()} style={styles.uploadButton}>
                <Image source={require('../../resource/upload.png')} style={{ width: 25, height: 25 }} />
            </TouchableOpacity>
            {data && <ImageView

                images={data}
                imageIndex={index}
                isVisible={visible}
                onClose={() => setVisible(false)}
                //renderFooter={() => (<View style={{width:'100%',height:50}}><Text >My footerasdasd</Text></View>)}
            />}
            {listPhoto.length > 0 && <FlatList
                data={listPhoto}
                numColumns={2}
                key={2}
                keyExtractor={(item) => "key" + item.id}
                onEndReachedThreshold={0.2}
                ListFooterComponent={renderFooter}
                onEndReached={loadMore}

                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { y: scrollY } } }],
                    { useNativeDriver: false }
                )}
                renderItem={({ item, index }) =>
                    <View style={{ flex: 1 }}>
                        <DreamItem openImage={() => openImage(index)} item={item} />
                    </View>
                }
            />}

        </View>
    )
}
const styles = StyleSheet.create({
    uploadButton: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 99
    }
})
export default DreamPage