export const API_END_POINT = "http://vhms.prod.vastbit.com:8001";
export const PUBLIC_END_POINT = "/api/mobile";
export const URL_SERVER = "http://storii.vastbit.com:2337/parse";
export const appId = "storii";
export const masterKey = "storiiii&1+1=10"
// 1. API LOGIN
export const API_LOGIN = API_END_POINT  + "/api/TokenAuth/Authenticate";

// 2. API GET USER PROFILE
export const API_GET_USER_PROFILE = API_END_POINT + PUBLIC_END_POINT + "/user/GetProfile";

// 3. API SIGN UP
export const API_SIGN_UP = API_END_POINT + PUBLIC_END_POINT + "/account/SignUp";

// 4. API GET ALL COUNTRY
export const API_GET_ALL_COUNTRY = API_END_POINT + "/api/services/app/Country/GetAllCountries";

// 5. API MAKE APPOINTMENT
export const API_GET_CITY_LOCATION = API_END_POINT +  PUBLIC_END_POINT + "/clinic/GetChildClinicsComboboxItems";
export const API_GET_SPECIALITY = API_END_POINT + PUBLIC_END_POINT + "/speciality/GetSpecialitiesComboboxItems";
export const API_GET_DOCTORS = API_END_POINT + PUBLIC_END_POINT + "/user/GetDoctorsComboboxItems";
export const API_GET_AVAILABLE_DATES = API_END_POINT + PUBLIC_END_POINT + "/workingschedule/GetAvailableDates"
export const API_GET_TIME_SLOTS = API_END_POINT + PUBLIC_END_POINT + "/workingschedule/GetAvailableTimeSlots"
// 6 . API GET LIST APPOINTMENT
export const API_GET_LIST_APPOINTMENT = API_END_POINT +  PUBLIC_END_POINT + "/appointment/GetAppointments";
// 7 . API CANCEL APPOINTMENT 
export const API_CANCEL_APPOINTMENT = API_END_POINT +  PUBLIC_END_POINT + "/appointment/CancelAppointment"
// 8. API CREATE APPOINTMENT
export const API_CREATE_APPOINTMENT = API_END_POINT + PUBLIC_END_POINT + "/appointment/CreateAppointment"
// 9. API GET COUNT APPOINTMENT
export const API_COUNT_APPOINTMENT = API_END_POINT + PUBLIC_END_POINT + "/appointment/GetCountAppointments"
// 10. API GET LIST EMR
export const API_GET_LIST_EMR = API_END_POINT + PUBLIC_END_POINT + "/orderitem/GetTestOrders" 
export const API_SAMPLE_RESULT_TEMPLATE = API_END_POINT + PUBLIC_END_POINT + "/orderitem/GetSampleResultTemplate"
export const API_HTML_TEMPLATE = API_END_POINT + PUBLIC_END_POINT + "/orderitem/GetHtmlTemplate"