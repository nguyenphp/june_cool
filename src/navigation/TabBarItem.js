import React, { PureComponent } from 'react';
import { Image, TouchableOpacity, StyleSheet,Text } from 'react-native';
import { normalize } from '../Services/ResizeFont';
import { Col, Button } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { mainColor, mainColorText, mainBackgroundColor } from '../Style/CustomStyle';
// import { Box, Text } from 'react-native-design-utility';

//import { tabBarIcons } from '../constants/images';

class TabBarItem extends PureComponent {
    handlePress = () => {
        this.props.navigation.navigate(this.props.routeName);
    };
    returnRouteName(name){
        if(name=="Home"){
            return "STO-TUBE"
        }else if(name=="HomePage"){
            return "HOME"
        }
        else if(name=="Sound"){
            return "STO-SOUND"
        }else if(name=="Notification"){
            return "NEWS"
        }else if(name=="Setting"){
            return "PROFILE"
        }
    }
    returnIcon(name){
        if(name=="Home"){
            return "ios-film"
        }else if(name=="Sound"){
            return "ios-musical-notes"
        }else if(name=="Notification"){
            return "ios-notifications"
        }else if(name=="Setting"){
            return "ios-person"
        }
    }
    render() {
        const { routeName, isActive } = this.props;

       // const icon = tabBarIcons[isActive ? 'active' : 'inactive'][routeName];
        return (
            //   <Box f={1} pt={10}>
            //     <TouchableOpacity onPress={this.handlePress} style={styles.button}>
            //       <Box mb={3}>
            //         <Image source={icon} />
            //       </Box>
            //       <Box>
            //         <Text size="xs" ls={0.12} color="greyDark" lowercase>
            //           {routeName}
            //         </Text>
            //       </Box>
            //     </TouchableOpacity>
            //   </Box>
            <Col style={styles.colItem}>
                <Button onPress={this.handlePress} style={[styles.buttonStyle,{borderBottomWidth:5,borderBottomColor:isActive?"#D51C29":mainBackgroundColor}]} transparent>
                    {/* <Ionicons style={[styles.iconStyle,{color:isActive?mainColor:"#7f8c8d"}]} name={this.returnIcon(routeName)} /> */}
                    <Text style={[styles.itemText]}>{this.returnRouteName(routeName)}</Text>
                </Button>
            </Col>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemText: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: normalize(10),

        fontWeight: '600',
        color:'white'
    },
    colItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    buttonStyle: { flexDirection: "column", justifyContent: 'flex-end', alignItems: 'center', width: '100%',marginBottom:10 },
    styleCenter: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyle: {
        color: mainColor,
        fontSize: normalize(22)
    },
});

export default TabBarItem;