import { View, Text, TouchableOpacity, SafeAreaView, Image, StyleSheet } from 'react-native';

import React, { Component } from 'react';
import { mainColor } from '../Styles/colors';
import LottieView from 'lottie-react-native';
import { connect } from 'react-redux'
import { pause, pointSong, deleteSong } from '../Actions/song'
class Tabbar extends Component<Props>  {
    componentDidMount() {
        this.animation.play();
        this.animation1.play();
        console.log("sadasd", this.props.currentSong)
        // this.animation2.play();
        // Or set a specific startFrame and endFrame with:
        // this.animation.play(30, 120);
    }

    resetAnimation = () => {
        this.animation.reset();
        this.animation.play();
    };
    playSong(isPlaying){
        this.props.onPlay()
    }
    render() {
        const { navigation, descriptors, state } = this.props;
        const focusedOptions = descriptors[state.routes[state.index].key].options;
        const currentSong = this.props.currentSong
        if (focusedOptions.tabBarVisible === false) {
            return null;
        }
        return (
            <SafeAreaView style={{ backgroundColor: "white" }}>
                {currentSong && <TouchableOpacity style={{ width: '100%', height: 50, backgroundColor: 'white',flexDirection:'row',alignItems:'center' }}
                onPress={()=>navigation.navigate("MusicPlayer")}
                >

                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Image style={{ width: 30, height: 30, borderRadius: 30 }} source={{ uri: currentSong.song_cover }} />
                    </View>
                    <View style={{ flex: 4, alignItems: 'flex-start' }}>
                        <Text style={{ color: 'black', fontFamily: 'SVN-HandOfSeanPro', fontSize: 15 }}>{currentSong.title}</Text>
                        <Text style={{ color: 'black', fontSize: 10, fontWeight: '200' }}>{currentSong.artist}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.props.onPlay()} style={{ flex: 1, alignItems: 'center' }}>
                        <Image style={{ width: 20, height: 20 }} source={!this.props.isPlaying?require('../../resource/play-button.png'):require('../../resource/pause-button.png')} />
                    </TouchableOpacity>
                </TouchableOpacity>}
                <View style={{ flexDirection: 'row', backgroundColor: "white", alignItems: 'center', height: 75, borderTopWidth: 0.8, borderColor: '#ecf0f1' }}>
                    {state.routes.map((route, index) => {
                        const { options } = descriptors[route.key];
                        const label =
                            options.tabBarLabel !== undefined
                                ? options.tabBarLabel
                                : options.title !== undefined
                                    ? options.title
                                    : route.name;
                        let icons = null;
                        let styleIcon = {};
                        const routeName = route.name;
                        switch (routeName) {
                            case "Story":
                                icons = require("../../resource/lottie/storyIcon.json");
                                styleIcon = { width: 26, height: 26 }
                                break;
                            case "Hình ảnh":
                                icons = require("../../resource/lottie/dreamIcon.json");
                                styleIcon = { width: 35, height: 20 }
                                break;


                            default:
                                break;
                        }

                        const isFocused = state.index === index;

                        const onPress = (label) => {
                            if (label == "Story") {
                                this.animation1.play();
                                this.animation2.reset();


                            } else if (label == "Hình ảnh") {
                                this.animation2.play();
                                this.animation1.reset();
                            }

                            const event = navigation.emit({
                                type: 'tabPress',
                                target: route.key,
                                canPreventDefault: true,
                            });

                            if (!isFocused && !event.defaultPrevented) {
                                navigation.navigate(route.name);
                            }
                        };

                        const onLongPress = () => {
                            navigation.emit({
                                type: 'tabLongPress',
                                target: route.key,
                            });
                        };

                        return (
                            <TouchableOpacity
                                key={"key" + index}
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={() => onPress(label)}
                                onLongPress={onLongPress}
                                style={{ flex: 1, alignItems: 'center', backgroundColor: "transparent" }}
                            >
                                {/* <Image style={[styleIcon,{margin: 5,tintColor: isFocused ? mainColor : textColor }]} source={icons} /> */}
                                {label === "MusicPage" ?
                                    <View>
                                        <LottieView
                                            ref={animation => {
                                                this.animation = animation;
                                            }}
                                            style={{
                                                width: 80,
                                                height: 80,
                                                //backgroundColor: 'black',
                                               // borderRadius: 40,
                                                marginBottom: 30
                                            }}
                                            source={require('../../resource/lottie/musicIcon.json')}
                                        // OR find more Lottie files @ https://lottiefiles.com/featured
                                        // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
                                        />
                                    </View>
                                    :
                                    <View style={{ alignItems: 'center' }}>
                                        {label === "Story" ? <LottieView
                                            ref={animation => {
                                                this.animation1 = animation;
                                            }}
                                            style={{
                                                width: 40,
                                                height: 40,
                                                backgroundColor: 'white',
                                                //borderRadius: 20,

                                            }}
                                            source={icons}
                                        // OR find more Lottie files @ https://lottiefiles.com/featured
                                        // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
                                        /> :
                                            <LottieView
                                                ref={animation => {
                                                    this.animation2 = animation;
                                                }}
                                                style={{
                                                    width: 40,
                                                    height: 40,
                                                    backgroundColor: 'white',
                                                   // borderRadius: 20,

                                                }}
                                                source={icons}
                                            // OR find more Lottie files @ https://lottiefiles.com/featured
                                            // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
                                            />
                                        }
                                        <Text style={{ color: isFocused ? "#e74c3c" : "black", fontSize: 15 }}>
                                            {label}
                                        </Text>
                                    </View>}
                            </TouchableOpacity>
                        );
                    })}
                </View>

            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    bottomTabStyle: {
        height: 80,
        flexDirection: 'row',
        //backgroundColor: '#676767',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center'
    }

});
const mapStateToProps = (state) => {
    return {
        playList: state.songs.playList,
        currentSong: state.songs.currentSong,
        isPlaying: state.songs.isPlaying,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPoint: (song) => {
            dispatch(pointSong((song)))
        },
        onDelete: (index) => {
            dispatch(deleteSong(index))
        },
        onPlay: () => {
            dispatch(pause())
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Tabbar)
