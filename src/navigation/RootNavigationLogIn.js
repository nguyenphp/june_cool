import React from 'react'
import {
    StyleSheet,
} from 'react-native'
import LoginScreen from "../Component/LoginScreen";
import { createStackNavigator } from "react-navigation";

export default RootStackLogged = createStackNavigator({

    LoginScreen: {
      screen: LoginScreen,
    },
   



  }, {
      initialRouteName: "LoginScreen",
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
        swipeEnabled: false,

      }
    })