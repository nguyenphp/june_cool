import React, { Component } from 'react';
import {
  StyleSheet, Text, View, Modal, SafeAreaView
} from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import StoryPage from '../Screens/StoryPage';
import MusicPage from '../Screens/MusicPage';
import DreamPage from '../Screens/DreamPage';
import Tabbar from './CustomTabBar';
import PlayerMusic from '../components/app/Player';
import { useNavigation } from '@react-navigation/native';

export const TRACKS = [
  {
    title: 'Hạnh ơi...',
    artist: 'A Chủn ft. A Củn',
    albumArtUrl: require("../../resource/hanhoi1.png"),
    audioUrl: "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3",
  },
  {
    title: 'Love Yourself',
    artist: 'Justin Bieber',
    albumArtUrl: "http://arrestedmotion.com/wp-content/uploads/2015/10/JB_Purpose-digital-deluxe-album-cover_lr.jpg",
    audioUrl: 'http://srv2.dnupload.com/Music/Album/Justin%20Bieber%20-%20Purpose%20(Deluxe%20Version)%20(320)/Justin%20Bieber%20-%20Purpose%20(Deluxe%20Version)%20128/05%20Love%20Yourself.mp3',
  },
  {
    title: 'Hotline Bling',
    artist: 'Drake',
    albumArtUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Drake_-_Hotline_Bling.png',
    audioUrl: 'http://dl2.shirazsong.org/dl/music/94-10/CD%201%20-%20Best%20of%202015%20-%20Top%20Downloads/03.%20Drake%20-%20Hotline%20Bling%20.mp3',
  },
];
const Tab = createBottomTabNavigator();
const BottomTab = () => {
  const [visible, setVisible] = React.useState(false)
  //const navigation = useNavigation()
  const onDownPress = () => {
    setVisible(false)
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Modal
        visible={visible}
        animationType="slide"
      >
        <View style={{ flex: 1 }}>
          <PlayerMusic onDownPress={()=>onDownPress()} tracks={TRACKS} />
        </View>
      </Modal>
      <View style={styles.bottomMusic}>
        <Text>asdasd</Text>
        <Text>asdasd</Text>
        <Text>asdasd</Text>
        </View>
      <Tab.Navigator tabBar={props => <Tabbar {...props} />}>
        <Tab.Screen name="Story" component={StoryPage} />
        <Tab.Screen
        //   listeners={{

        //     tabPress: e => {
        //         setVisible(true)
        //         e.preventDefault();
        //         //navigation.navigate("MusicPage")
        //     },
        // }}
        name="MusicPage" component={MusicPage} />
        <Tab.Screen name="Hình ảnh" component={DreamPage} />
      </Tab.Navigator>
    </SafeAreaView>
  )
}
export default BottomTab;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  itemText: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: '600'
  },
  bottomMusic:{
    height:80,
    backgroundColor:'red',
    width:'100%',
    position:'absolute',
    bottom:70,
    left:0,
    right:0,

  }
});