/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator,TransitionPresets } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MusicPlayer from '../Screens/MusicPlayer';
import BottomTab from './TabBarNavigation';
import AddStoryScreen from '../Screens/AddStoryScreen';
import EditStoryScreen from '../Screens/EditStoryScreen';

const Stack = createStackNavigator();
const RootNavigation = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
        
          <Stack.Screen name="BottomTab" component={BottomTab} />
          <Stack.Screen  name="MusicPlayer" component={MusicPlayer}
          options={{
            title: 'MusicPlayer',
            ...TransitionPresets.ModalSlideFromBottomIOS,
          }}
          />
          <Stack.Screen name="AddStoryScreen" component={AddStoryScreen} 
          options={{
            title: 'AddStoryScreen',
            ...TransitionPresets.ModalSlideFromBottomIOS,
          }}
          />
           <Stack.Screen name="EditStoryScreen" component={EditStoryScreen} 
          options={{
            title: 'EditStoryScreen',
            ...TransitionPresets.ModalSlideFromBottomIOS,
          }}
          />
          
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({


});

export default RootNavigation;
