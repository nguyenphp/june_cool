/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, AlertIOS, TouchableWithoutFeedback } from 'react-native';
import { Row, Input, Button, Item, Col, Card, CardItem, ListItem, Left, Body, Thumbnail, Right } from 'native-base';
import HeaderNavigation from './HeaderNavigation';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const logo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import DeviceInfo from 'react-native-device-info'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import moment from 'moment';
import { normalize } from '../Services/ResizeFont';
import mainColor from '../Style/CustomStyle'
import Credentials from '../Services/Credentials';
import { I18nManager, AsyncStorage } from "react-native";
;
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from 'react-native-vector-icons/FontAwesome'
import { Avatar } from 'react-native-elements';

//import RNRestart from "react-native-restart";
type Props = {};
// let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
// const userCountryData = getAllCountries()
// console.log(userCountryData)
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
export default class NotFoundUI extends Component<Props> {
    constructor(props) {
        //StatusBarIOS.setHidden(true)
        super(props)

        this.state = {
            isLogin: true,
            isDateTimePickerVisible: false,
            paused: false,
            progress: 0,
            duration: 0,
            isFullscreen: false,
            showFullScreen: false,
            paused1: false,
            isLoading: false,
            listings: []

        }
    }
   
    render() {
      
        return (
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>

                <Image style={{width:250,height:250}} source={require('../../resource/notfound.png')}/>
                <Text style={styles.textMain}>Không tìm thấy kết quả</Text>
                <Text style={styles.textSub}>Hãy thử các từ khoá khác nhau hoặc xoá bộ lọc tìm kiếm xem thế nào nhé</Text>

            </View>
        );
    }
}
NotFoundUI.propTypes = {
  
    name: PropTypes.string,
    notification: PropTypes.string,
    openVideo: PropTypes.any
}
NotFoundUI.defaultProps = {
    title: 'Heading One'
}
const styles = StyleSheet.create({
   textMain:{
       fontSize:normalize(16),
       fontWeight:'bold'
   },
   textSub:{
    fontSize:normalize(12),
    //fontWeight:'bold',
    color:'#bdc3c7',
    width:'90%',
    textAlign:'center'
   }
});
