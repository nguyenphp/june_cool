/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, AlertIOS, TouchableWithoutFeedback, Modal,TouchableOpacity } from 'react-native';
import { Row, Input, Button, Item, Col, Card, CardItem, ListItem, Left, Body, Thumbnail, Right, Spinner } from 'native-base';
import HeaderNavigation from './HeaderNavigation';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const logo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import DeviceInfo from 'react-native-device-info'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import moment from 'moment';
import { normalize } from '../Services/ResizeFont';
import mainColor from '../Style/CustomStyle'
import Credentials from '../Services/Credentials';
import { I18nManager, AsyncStorage } from "react-native";
import Share, {ShareSheet} from 'react-native-share';
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from 'react-native-vector-icons/FontAwesome'
import { Avatar, SocialIcon } from 'react-native-elements';

//import RNRestart from "react-native-restart";
type Props = {};
// let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
// const userCountryData = getAllCountries()
// console.log(userCountryData)
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
export default class ModalShare extends Component<Props> {
    constructor(props) {
        //StatusBarIOS.setHidden(true)
        super(props)

        this.state = {
            isLogin: true,
            isDateTimePickerVisible: false,
            paused: false,
            progress: 0,
            duration: 0,
            isFullscreen: false,
            showFullScreen: false,
            paused1: false,
            isLoading: false,
            listings: [],
            modalShare:false

        }
    }
    componentDidUpdate(prevProps){
        if(prevProps.visible!=this.props.visible){
            this.setState({
                modalShare:this.props.visible
            })
        }
    }
    render() {

        return (

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <TouchableWithoutFeedback onPress={() => this.props.parent.closeModal(false)}>
                    <View style={styles.shareScreenAll}>
                        <View style={styles.shareScreen}>
                            <View style={styles.headerShare}>
                                <Text style={styles.titleStyle}>Chia sẻ</Text>
                            </View>
                            <View style={styles.socialMediaContainer}>
                                {/* <TouchableOpacity style={[styles.rosterStyle, { marginRight: 25 }]} onPress={() => {
                                    this.setModalVisibleShareInternal(true)
                                }}>
                                   
                                    <Image style={{ width: 35, height: 35 }} source={require("../../resource/logo.png")} />
                                
                                </TouchableOpacity> */}
                                <View>
                                <TouchableOpacity style={[styles.linkStyle, { marginRight: 15,backgroundColor:'#2980b9' }]} onPress={() => {
                                {/* <SocialIcon style={{ marginRight: 15 }} type='instagram' onPress={() => { */}
                                    this.onCancel();
                                    let shareOptions = {
                                        title: "News from RosterSpot",
                                        message: this.props.post.body,
                                        url: this.props.post.url == null ? "http://twitter.com" : this.props.post.url,
                                        subject: "News from RosterSpot",
                                        //social: Share.Social.TWITTER
                                    };
                                    setTimeout(() => {
                                        Share.shareSingle(Object.assign(shareOptions, {
                                            "social": "twitter"
                                        })).then((res: any) => {
                                            console.log(res);

                                            this.props.action.sharePost(this.props.post.id);
                                            if (this.props.post.author.user_id != this.props.profileUser.id) {
                                                let postData = {
                                                    "title": "shared your post",
                                                    "content": this.props.post.author.name + " have new share",
                                                    "type": 1,
                                                    "screen": "rosterspot.PublicProfileScreen",
                                                    "to_user_id": this.props.post.author.user_id
                                                }
                                                this.props.action.postAlert(postData);
                                            }
                                        })
                                            .catch((err: any) => {

                                                err && err.error !== "User did not share" && Alert.alert("Share failed", err.error);
                                            });
                                    }, 300);
                                }}
                                >
                                    <FontAwesome
                                        style={styles.iconLink}
                                        size={24}
                                        name="instagram"
                                    />
                                </TouchableOpacity>
                                <Text style={styles.textShareIcon}>Instagram</Text>
                                </View>
                                <View>
                                <TouchableOpacity style={[styles.linkStyle, { marginRight: 15,backgroundColor:'#4267b2' }]} onPress={() => {
                                // <SocialIcon style={{ marginLeft: 10, marginRight: 15 }} type='facebook' onPress={() => {
                    this.onCancel();
                    let shareOptions = {
                        title: "News from RosterSpot",
                        message: this.props.post.body,
                        url: this.props.post.url == null ? "http://facebook.com" : this.props.post.url,
                        subject: "News from RosterSpot",
                       // social: Share.Social.FACEBOOK
                    };
                    console.log("share facebook")
                    console.log(shareOptions);
                    setTimeout(() => {
                        Share.shareSingle(Object.assign(shareOptions, {
                            "social": "facebook"
                        })).then((res: any) => {
                            console.log(res);

                        })
                            .catch((err: any) => {

                                err && console.log(err);
                            });
                    }, 300);
                }
                }
                >
                    <FontAwesome
                                        style={styles.iconLink}
                                        size={24}
                                        name="facebook"
                                    />
                </TouchableOpacity>
                <Text style={styles.textShareIcon}>Facebook</Text>
                </View>
                          
                                <View style={styles.iconShareArea}>
                                <TouchableOpacity style={[styles.linkStyle, { marginRight: 15 }]} onPress={() => {
                                    let shareOptions = {
                                        title: "News from RosterSpot",
                                        message: this.props.post.body,
                                        url: this.props.post.url,
                                        subject: "News from RosterSpot",

                                    };
                                    this.writeToClipboard(shareOptions.url);
                                    this.props.parent.copied()
                                }}>
                                    {/* <View> */}
                                    <Icon
                                        style={styles.iconLink}
                                        size={24}
                                        name="link"
                                    />
                                    {/* </View> */}
                                </TouchableOpacity>
                                <Text style={styles.textShareIcon}>Link</Text>
                                </View>
                                <View>
                                <TouchableOpacity style={[styles.linkStyle, { marginRight: 15,backgroundColor:'#7f8c8d' }]} onPress={() => {
                                    let shareOptions = {
                                        title: "News from Storii",
                                        message:"test",
                                        url: "http://google.com",
                                        subject: "News from storii",

                                    };
                                    this.props.parent.closeModal(false)
                                    setTimeout(() => {
                                        Share.open(shareOptions)
                                      },300);
                                }}>
                                   
                                    <FontAwesome style={styles.iconLink}
                                        size={24} name="ellipsis-h"/>
                                    {/* </View> */}
                                </TouchableOpacity>
                                <Text style={styles.textShareIcon}>More</Text>
                                </View>
                             
                            </View>
                            <View style={{width:'100%',borderTopWidth:0.5,borderColor: '#ecf0f1'}}>
                                <Button style={{width:'100%',justifyContent:'center',alignItems:'center'}} onPress={() => this.props.parent.closeModal(false)} transparent>
                                    <Text style={styles.titleStyle}>Huỷ</Text></Button>
                            </View>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>

        );
    }
}
ModalShare.propTypes = {

    visible: PropTypes.bool,
    parent: PropTypes.any

}
ModalShare.defaultProps = {
    visible: false
}
const styles = StyleSheet.create({
    commentsContainer: {
        marginTop: 20
    },
    commentPostActionContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    launchCommentModalContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    postActionsContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#EFEFF4',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 5,
    },
    fistbumps: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    fistbumpIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73,
        width: 40,
        height: 40,
    },
    fistbumpsText: {
        color: '#47535f',
        fontSize: 13,
    },
    comments: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2
    },
    commentsIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73
    },
    commentsText: {
        color: '#47535f',
        fontSize: 13,
    },
    shares: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        zIndex: 1,

    },
    shareIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73
    },
    sharesText: {
        color: '#47535f',
        fontSize: 13,
    },
    shareScreen: {
        width: '100%',
        height: 200,
        backgroundColor: 'white',
        position: 'absolute', bottom: 0

    },
    shareScreenAll: {
        width: '100%', height: '100%',
        backgroundColor: 'rgba(52, 52, 52, 0.8)'
    },

    headerShare: {

        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ecf0f1'
    },
    titleStyle: {
        fontSize: normalize(14),
        fontWeight: '300',
        textAlign:"left"

    },
    colShare: {
        justifyContent: 'center', alignItems: 'center'
    },
    socialMediaContainer: {
        width: '100%',
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
    },

    linkStyle: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 52,
        height: 52,
        backgroundColor: '#bdc3c7',
        borderRadius: 25.5,
        //borderWidth:0.5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                elevation: 2,
            },
        }),
    },
    rosterStyle: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 52,
        height: 52,
        backgroundColor: '#231F20',
        borderRadius: 25.5,
        //borderWidth:0.5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                elevation: 2,
            },
        }),
    },
    iconLink: {
        color: 'white',

    },
    headerShareInternal: {
        height: 60,
        justifyContent: 'center',
        //alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ecf0f1'
    },
    textStyle: {
        fontSize: 16, color: 'black'
    },
    bodyShareInternal: {
        height: 140
    },
    buttonShareInternal: {
        height: 40,
        width: '100%'
    },
    buttonShareNow: { height: 40, width: '100%', backgroundColor: '#FFF200', justifyContent: 'center', alignItems: 'center' },
    buttonTextShareNow: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#231F20',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    postTextBox: {
        flex: 1,
        marginTop: 20,
        marginLeft: 15,
    },
    iconShareArea:{justifyContent:'center'},
    textShareIcon:{
        fontSize:normalize(9),
        fontWeight:'200',
        width:70,
        textAlign:'center',
        marginTop:5
    }



});
