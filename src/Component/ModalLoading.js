/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, AlertIOS, TouchableWithoutFeedback,Modal } from 'react-native';
import { Row, Input, Button, Item, Col, Card, CardItem, ListItem, Left, Body, Thumbnail, Right, Spinner } from 'native-base';
import HeaderNavigation from './HeaderNavigation';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const logo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import DeviceInfo from 'react-native-device-info'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';


//import RNRestart from "react-native-restart";
type Props = {};
// let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
// const userCountryData = getAllCountries()
// console.log(userCountryData)
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
export default class ModalLoading extends Component<Props> {
    constructor(props) {
        //StatusBarIOS.setHidden(true)
        super(props)

        this.state = {
            isLogin: true,
            isDateTimePickerVisible: false,
            paused: false,
            progress: 0,
            duration: 0,
            isFullscreen: false,
            showFullScreen: false,
            paused1: false,
            isLoading: false,
            listings: []

        }
    }

    render() {
     
        return (
            <Modal
            visible={this.props.visible}
            transparent
            >
                <View style={{flex:1,backgroundColor:'rgba(189, 195, 199,0.0)',justifyContent:'center',alignItems:'center'}}>
                    <View style={{width:100,height:100,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(236, 240, 241,1.0)',borderRadius:10,borderWidth:0.2,borderColor:'rgba(189, 195, 199,0.7)'}}>
                    <Spinner/>
                    </View>
                    
                </View>
            </Modal>
        );
    }
}
ModalLoading.propTypes = {
  
    visible: PropTypes.bool,
  
}
ModalLoading.defaultProps = {
    visible: false
}
const styles = StyleSheet.create({
    container: {
        
      },
      alertContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        padding: 10,
        backgroundColor: 'rgba(236, 240, 241,0.5)',
        borderBottomWidth:0.5,
        borderColor: 'rgba(189, 195, 199,0.5)',
        //borderWidth: 1,
        borderRadius: 5,

      },
      alertDetails: {
        flex: 3,
        paddingLeft: 5,
        flexDirection: 'column'
      },
      alertTime: {
        flex: .75,
        justifyContent: 'center',
        alignItems: 'center',
      },
      circularProfile: {
        flex: 1,
        borderWidth: 3,
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      name: {
        color: '#2A2526',
        fontSize: 15.3,
        fontWeight: 'bold',
      },
      title: {
        color: '#2A2526',
        fontSize: 15.3,
       
      },
      content: {
        fontSize: 13.1,
        color: '#969696',
      },
      time: {
        fontSize: 10.1,
        color: '#969696',
      },
      delete:{
        flex:0.55,
        alignItems:'center',
        justifyContent:'center',
        marginLeft:30
       
      
      }

});
