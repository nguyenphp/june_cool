/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

import PropTypes from 'prop-types';
import {mainColor, mainBackgroundColor, mainColorText} from '../Style/CustomStyle';
import { Col, Row, Icon, Button, ListItem } from 'native-base';
import { normalize } from '../Services/ResizeFont';
import { MyStatusBar } from './MyStatusBar';
import SvgUri from 'react-native-svg-uri';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {
  //title
};

export default class HeaderNavigationTransparent extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ backgroundColor: mainBackgroundColor }}>
        {/* <MyStatusBar backgroundColor={mainColor} barStyle='light-content' /> */}
        </SafeAreaView>
        {/* <View style={styles.container}> */}
        
          <Row style={{width:'90%',borderBottomWidth:0.3,borderColor:'white'}}>
            <Col style={{width:50}}>
            {/* {!this.props.isBack ?  <Button onPress={() => { this.props.parent.goNotification() }} transparent><Icon style={styles.iconStyle} name='ios-notifications' /></Button>:<Button/>} */}
            {this.props.isBack ?  <Button onPress={() => { this.props.parent.goBack() }} transparent><Icon onPress={() => { this.props.parent.goBack() }} style={styles.iconStyle} name='arrow-back' /></Button>:<Button/>}
              </Col>
            <Col style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
            {this.props.isIcon &&
                  <SvgUri width="40" height="30" source={this.props.sourceIcon} />
              }
              {this.props.isIcon==false && <ListItem style={{ borderBottomWidth: 0 }}>
                <Text style={styles.welcome}>{this.props.title}</Text>
                <SvgUri style={{ marginBottom: 10, marginLeft: 5 }} width="26" height="22" source={require('../../resource/logoIcon.svg')} />
              </ListItem>}
            </Col>
            <Col style={styles.colLeft}> 
            <Button onPress={() => { this.props.parent.goBack() }} transparent>
           {this.props.isSearch ? <Icon onPress={()=>{this.props.parent.showSearch(true)}} style={styles.iconStyle}  name="search" size={30} color="#FFF" />:<Button/>}
            </Button></Col>
          </Row>
        {/* </View> */}
      </View>
    );
  }
}
HeaderNavigationTransparent.propTypes = {
  title: PropTypes.string,
  navigation: PropTypes.any,
  isBack: PropTypes.bool,
  isSearch:PropTypes.bool,
  showSearch: PropTypes.any,
  isIcon : PropTypes.any,
  sourceIcon:PropTypes.any
}
HeaderNavigationTransparent.defaultProps = {
  title: 'Heading One',
  isIcon: false
}
const styles = StyleSheet.create({
  container: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: mainColor,
    zIndex:100,
    position:'absolute',
    top: 0,
    left:0,
    right:0,
    backgroundColor:'transparent',
    // borderBottomWidth:0.5,
    // borderColor:mainColorText
   
  },
  colLeft: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  welcome: {
    fontSize: normalize(12),
    textAlign: 'center',
   //margin: 10,
    color: mainColorText,
    fontWeight:'600',
    fontFamily:"HelveticaNeueLTStd-Md",
    marginTop:5,
    marginLeft:26
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  iconStyle: {
    color: 'white',
    fontSize: 25
  }
});
