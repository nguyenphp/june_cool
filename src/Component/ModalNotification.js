/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, AlertIOS, TouchableWithoutFeedback,Modal,FlatList } from 'react-native';
import { Row, Input, Button, Item, Col, Card, CardItem, ListItem, Left, Body, Thumbnail, Right, Spinner } from 'native-base';
import HeaderNavigation from './HeaderNavigation';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const logo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import DeviceInfo from 'react-native-device-info'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import moment from 'moment';
import { normalize } from '../Services/ResizeFont';
import mainColor from '../Style/CustomStyle'
import Credentials from '../Services/Credentials';
import { I18nManager, AsyncStorage } from "react-native";
;
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from 'react-native-vector-icons/FontAwesome'
import { Avatar } from 'react-native-elements';
import NotificationScreen from './NotificationScreen';
import NotificationItem from './NotificationItem';
import { connect } from 'react-redux';
import Actions from '../Actions';
import Selectors from '../Selectors';
import ModalLoading from './ModalLoading';
//import RNRestart from "react-native-restart";
type Props = {};
// let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
// const userCountryData = getAllCountries()
// console.log(userCountryData)
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
class ModalNotification extends Component<Props> {
    constructor(props) {
        //StatusBarIOS.setHidden(true)
        super(props)

        this.state = {
            isLogin: true,
            isDateTimePickerVisible: false,
            paused: false,
            progress: 0,
            duration: 0,
            isFullscreen: false,
            showFullScreen: false,
            paused1: false,
            isLoading: false,
            listings: []

        }
    }
    
    openDrawer() {
        this.props.navigation.openDrawer();
    }
    async componentWillMount(){
       
    }
    componentDidMount() {
        this.props.getListNotification("")
        this.setState({
            loading:true
        })
    }
    componentDidUpdate(prevProps){
        if(prevProps.listNotification!=this.props.listNotification){
            this.setState({
                listNotification:this.props.listNotification,
                loading:false
            })
        }
    }
    goBack(){
        this.props.parent.closeNoti()
    }
    render() {
     
        return (
            <Modal
            visible={this.props.visible}
            //transparent
            >
                <View style={styles.container}>
                <ModalLoading visible={this.state.loading}/>
                <HeaderNavigation isBack={true} isDrawer={true} parent={this} title={"THÔNG BÁO"} />
                <Row style={styles.rowLogin}>
                    {/* <BillingItem parent={this} />
                 */}
                   <FlatList
                   data={this.state.listNotification}
                   keyExtractor={(item, index) => 'key' + index}
                   renderItem={({item})=>(
                       <NotificationItem createdAt={item.createdAt} name={item.title} notification={item.content}/>
                   )}
                   />
                </Row>
            </View>
            </Modal>
        );
    }
}
ModalNotification.propTypes = {
  
    visible: PropTypes.bool,
    parent: PropTypes.any
  
}
ModalNotification.defaultProps = {
    visible: false
}
function mapStateToProps(state, props) {
    return {

        listNotification : Selectors.NotificationScreen.listNotification(state)
       

    }
}
function mapDispatchToProps(dispatch) {
    return {

        logOut: () => dispatch(Actions.LoginScreen.logOut()),
        getListNotification: (userId)=> dispatch(Actions.NotificationScreen.getListNotification(userId))



    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalNotification);
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    welcome: {
        fontSize: 17,
        textAlign: 'center',
        margin: 10,
    },
    textSub: {
        fontSize: 12,
        color: 'rgba(30, 39, 46,0.7)'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    rowWelcome: {
        flex: 0.4,
        marginTop: 20,
        // flexDirection: 'row',
        alignItems: 'center'
    },
    rowText: {
        flex: 0.05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowBottom: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 50,
        left: '5%',
        right: '5%'


    },
    rowTop: {
        flex: 0.2
    },
    buttonLogin: {
        width: '90%',
        backgroundColor: mainColor,
        borderRadius: 10,
        height: 55,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        color: 'white',
        fontSize: 14
    },
    subTextStyle:{
        fontStyle: 'italic',
        fontSize:12
    }
});
