/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, View, Image ,Alert,FlatList} from 'react-native';
import HeaderNavigation from './HeaderNavigation';
import { SafeAreaView } from 'react-navigation';
import { Row, Button, Content, ListItem, Left, Thumbnail, Body, Right, Text } from 'native-base';

import { UserProfile } from '../DataObjects/UserProfile';
import mainColor from '../Style/CustomStyle'
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
const logo = require("../../resource/logo.png")
import Credentials from '../Services/Credentials';
import { I18nManager, AsyncStorage } from "react-native";
;
import NotificationItem from './NotificationItem';
import { connect } from 'react-redux';
import Actions from '../Actions';
import Selectors from '../Selectors';
import ModalLoading from './ModalLoading';
//import RNRestart from "react-native-restart";
type Props = {};
let dataNotification = [
    {
        "name":"Thông báo mới",
        "notification":"Vừa có video clip mới kìa click xem ngay nào các bạn"
    },
    {
        "name":"Thông báo mới",
        "notification":"Trong vòng vài phút nữa Hà Anh Tuấn sẽ livestream"
    },
    {
        "name":"Thông báo mới",
        "notification":"act cool bạn có muốn tham gia ngay làm fan của Phúc Nguyên không"
    },
    {
        "name":"Thông báo mới",
        "notification":"Bạn vừa có thêm một người theo dõi"
    },
    {
        "name":"Thông báo mới",
        "notification":"Hà Anh Tuấn vừa ra MV mới click ngay nào!!!!"
    }
]
class NewsScreen extends Component<Props> {
    constructor(props) {
        super(props)
        this.state = {
            userProfile: new UserProfile(),
            loading:false
        }
    }
    openDrawer() {
        this.props.navigation.openDrawer();
    }
    async componentWillMount(){
       
    }
    componentDidMount() {
        this.props.getListNotification("")
        this.setState({
            loading:true
        })
    }
    componentDidUpdate(prevProps){
        if(prevProps.listNotification!=this.props.listNotification){
            this.setState({
                listNotification:this.props.listNotification,
                loading:false
            })
        }
    }
    logOut(){
         
        Alert.alert("Log Out",
            "Do you really want to log out?",
            [

                {
                    text: 'Cancel',
                    onPress: () => console.log("log out"),
                    style: 'cancel',
                },
                {
                    text: 'OK', onPress: () => {
                        this.props.navigation.replace('LoginScreen')
                        Credentials.logOut();
                    }
                },
            ],
            { cancelable: false },
        );
    }
    render() {
       
        return (
            <View style={styles.container}>
                <ModalLoading visible={this.state.loading}/>
                <HeaderNavigation isDrawer={true} parent={this} title={"NEWS"} />
                {/* <Row style={styles.rowLogin}>
                   
                   <FlatList
                   data={this.state.listNotification}
                   keyExtractor={(item, index) => 'key' + index}
                   renderItem={({item})=>(
                       <NotificationItem createdAt={item.createdAt} name={item.title} notification={item.content}/>
                   )}
                   />
                </Row> */}
            </View>
        );
    }
}
function mapStateToProps(state, props) {
    return {

        listNotification : Selectors.NotificationScreen.listNotification(state)
       

    }
}
function mapDispatchToProps(dispatch) {
    return {

        logOut: () => dispatch(Actions.LoginScreen.logOut()),
        getListNotification: (userId)=> dispatch(Actions.NotificationScreen.getListNotification(userId))



    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    welcome: {
        fontSize: 17,
        textAlign: 'center',
        margin: 10,
    },
    textSub: {
        fontSize: 12,
        color: 'rgba(30, 39, 46,0.7)'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    rowWelcome: {
        flex: 0.4,
        marginTop: 20,
        // flexDirection: 'row',
        alignItems: 'center'
    },
    rowText: {
        flex: 0.05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowBottom: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 50,
        left: '5%',
        right: '5%'


    },
    rowTop: {
        flex: 0.2
    },
    buttonLogin: {
        width: '90%',
        backgroundColor: mainColor,
        borderRadius: 10,
        height: 55,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        color: 'white',
        fontSize: 14
    },
    subTextStyle:{
        fontStyle: 'italic',
        fontSize:12
    }
});
