/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, AlertIOS, TouchableWithoutFeedback, Modal, TouchableOpacity, StatusBar,ImageBackground } from 'react-native';
import { Row, Input, Button, Item, Col, Card, CardItem, ListItem, Left, Body, Thumbnail, Right, Spinner } from 'native-base';
import HeaderNavigation from './HeaderNavigation';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const logo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import DeviceInfo from 'react-native-device-info'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import moment from 'moment';
import { normalize } from '../Services/ResizeFont';
import mainColor from '../Style/CustomStyle'
import Credentials from '../Services/Credentials';
import { I18nManager, AsyncStorage } from "react-native";
;
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from 'react-native-vector-icons/FontAwesome'
import { Avatar, SocialIcon } from 'react-native-elements';
import SeekBar from '../CustomPlayer/SeekBar';
import Header from '../CustomPlayer/Header';
import AlbumArt from '../CustomPlayer/AlbumArt';
import TrackDetails from '../CustomPlayer/TrackDetails';
import Controls from '../CustomPlayer/Controls';
import { SafeAreaView } from 'react-navigation';
import { pause, seekProgress, nextSong, lastSong, switchMode } from '../Actions/song';
// import Video from 'react-native-video';

import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import Actions from '../Actions';
import Selectors from '../Selectors';

//import RNRestart from "react-native-restart";
type Props = {};
// let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
// const userCountryData = getAllCountries()
// console.log(userCountryData)
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
class MusicPlayer extends Component<Props> {
    constructor(props) {
        //StatusBarIOS.setHidden(true)
        super(props)

        this.state = {
            isLogin: true,
            isDateTimePickerVisible: false,
            paused: false,
            progress: 0,
            duration: 0,
            isFullscreen: false,
            showFullScreen: false,
            paused1: false,
            isLoading: false,
            listings: [],
            modalShare: false

        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.visible != this.props.visible) {
            this.setState({
                modalShare: this.props.visible
            })
        }
    }
    setDuration(data) {
        // console.log(totalLength);
        this.setState({totalLength: Math.floor(data.duration)});
      }
    
      setTime(data) {
        //console.log(data);
        this.setState({currentPosition: Math.floor(data.currentTime)});
      }
    
      seek(time) {
        time = Math.round(time);
        this.props.onSeek(time);
        // this.refs.audioElement && this.refs.audioElement.seek(time);
        // this.setState({
        //   currentPosition: time,
        //   paused: false,
        // });
      }
    
      onBack() {
        
        this.props.prevSong(this.props.isFinish)
      }
    
      onForward() {
       
        this.props.nextSong(this.props.isFinish)
      }
      componentDidUpdate(prevProps){
        if(prevProps.currentPosition!=this.props.currentPosition){
          this.setState({
            currentPosition:this.props.currentPosition
          })
        }
      }
      switchMode(){
        this.setState({ repeatOn: !this.state.repeatOn},()=>this.props.onSwitchMode())
      }
    render() {
       // console.log("total length ", this.props.totalLength)
        return (

            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                {/* <TouchableWithoutFeedback onPress={() => this.props.parent.closeModal(false)}> */}
                <View style={styles.shareScreenAll}>
                    <ImageBackground style={{width:'100%',height:'100%'}} resizeMode="cover" source={{uri:this.props.urlThumbnail}}>
                    <SafeAreaView />
                    <StatusBar hidden={false} backgroundColor="blue" barStyle="light-content" />
                    <Header onDownPress={() => this.props.onDownPress(this.state.currentPosition)} message="Playing From Charts" />
                    <AlbumArt url={this.props.urlThumbnail} lyrics={this.props.lyrics} />
                    <TrackDetails title={this.props.title} artist={this.props.artists} />
                    <SeekBar
                        onSeek={this.seek.bind(this)}
                        trackLength={this.props.totalLength}
                        onSlidingStart={() => this.setState({ paused: true })}
                        currentPosition={this.props.currentTime} />
                    <Controls
                        onPressRepeat={() => this.switchMode()}
                        repeatOn={!this.state.repeatOn}
                        shuffleOn={this.state.shuffleOn}
                        //forwardDisabled={this.state.selectedTrack === this.props.tracks.length - 1}
                        onPressShuffle={() => this.setState({ shuffleOn: !this.state.shuffleOn })}
                        onPressPlay={() => this.props.onPlay()}
                        onPressPause={() => this.props.onPlay()}
                        onBack={this.onBack.bind(this)}
                        onForward={this.onForward.bind(this)}
                        paused={!this.props.isPlaying} />
                        </ImageBackground>
                </View>
                {/* </TouchableWithoutFeedback> */}
            </Modal>

        );
    }
}
MusicPlayer.propTypes = {
    currentTime: PropTypes.any,
    totalLength: PropTypes.any,
    visible: PropTypes.any,
    parent: PropTypes.any,
    urlThumbnail: PropTypes.any,
    title: PropTypes.any,
    artists: PropTypes.any,
    onDownPress: PropTypes.any,
    lyrics:PropTypes.any,
    

}
MusicPlayer.defaultProps = {
    visible: false
}
function mapStateToProps(state, props) {
    return {

        isPlaying: state.songs.isPlaying,
        currentSong: state.songs.currentSong,
        isFinish: state.songs.isFinish,
        playMode: state.songs.playMode,

    }
}
function mapDispatchToProps(dispatch) {
    return {
        onPlay: () => {
            dispatch(pause())
        },
        onSeek:(time)=>{
            dispatch(seekProgress(time))
        },
        nextSong:(isFinish)=>{
            dispatch(nextSong(isFinish))
        },
        prevSong:(isFinish)=>{
            dispatch(lastSong(isFinish))
        },
        onSwitchMode: () => {
            dispatch(switchMode())
        }
      



    }
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(MusicPlayer);
const styles = StyleSheet.create({
    commentsContainer: {
        marginTop: 20
    },
    commentPostActionContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    launchCommentModalContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    postActionsContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#EFEFF4',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 5,
    },
    fistbumps: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    fistbumpIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73,
        width: 40,
        height: 40,
    },
    fistbumpsText: {
        color: '#47535f',
        fontSize: 13,
    },
    comments: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2
    },
    commentsIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73
    },
    commentsText: {
        color: '#47535f',
        fontSize: 13,
    },
    shares: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        zIndex: 1,

    },
    shareIcon: {
        marginRight: 5,
        resizeMode: 'contain',
        aspectRatio: 0.73
    },
    sharesText: {
        color: '#47535f',
        fontSize: 13,
    },
    shareScreen: {
        width: '100%',
        height: 140,
        backgroundColor: 'white',
        position: 'absolute', bottom: 0

    },
    shareScreenAll: {
        width: '100%', height: '100%',
        backgroundColor: 'rgb(4,4,4)'
    },

    headerShare: {

        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ecf0f1'
    },
    titleStyle: {
        fontSize: 18,
        fontWeight: 'bold'

    },
    colShare: {
        justifyContent: 'center', alignItems: 'center'
    },
    socialMediaContainer: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
    },

    linkStyle: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 52,
        height: 52,
        backgroundColor: '#bdc3c7',
        borderRadius: 25.5,
        //borderWidth:0.5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                elevation: 2,
            },
        }),
    },
    rosterStyle: {
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: 52,
        height: 52,
        backgroundColor: '#231F20',
        borderRadius: 25.5,
        //borderWidth:0.5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0, .4)',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
            },
            android: {
                elevation: 2,
            },
        }),
    },
    iconLink: {
        color: 'white',

    },
    headerShareInternal: {
        height: 60,
        justifyContent: 'center',
        //alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#ecf0f1'
    },
    textStyle: {
        fontSize: 16, color: 'black'
    },
    bodyShareInternal: {
        height: 140
    },
    buttonShareInternal: {
        height: 40,
        width: '100%'
    },
    buttonShareNow: { height: 40, width: '100%', backgroundColor: '#FFF200', justifyContent: 'center', alignItems: 'center' },
    buttonTextShareNow: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#231F20',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    postTextBox: {
        flex: 1,
        marginTop: 20,
        marginLeft: 15,
    },



});
