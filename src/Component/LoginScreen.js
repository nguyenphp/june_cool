/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Alert, Modal, ScrollView, KeyboardAvoidingView, TouchableHighlight, TextInput, NetInfo, AsyncStorage } from 'react-native';
import { Row, Input, Button, Icon, Item, Col, Spinner, ListItem } from 'native-base';
import { createStackNavigator, NavigationActions } from 'react-navigation';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import Actions from '../Actions';
import Selectors from '../Selectors';
import { normalize } from '../Services/ResizeFont';
const logo = require("../../resource/logo.png")
import { mainColor, mainColorText, mainBackgroundColor, mainBackgroundColorButton } from '../Style/CustomStyle'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { SocialIcon } from 'react-native-elements';
let appLogo = require("../../resource/logo.png")
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
import Parse from 'parse/react-native';
import ParseReact from 'parse-react/react-native';
import SvgUri from 'react-native-svg-uri';
import { google, facebook, twitter, tumblr } from 'react-native-simple-auth';
type Props = {
    login: (loginUser) => void
};
// const navigateAction = NavigationActions.navigate({
//     routeName: 'HomeScreen',
//     params: {},

//     // navigate can have a nested navigate action that will be run inside the child router
//     //action: NavigationActions.replace({ routeName: 'SubProfileRoute' }),
// });
//let Navigation = createAppContainer(RootStack);
import { URL_SERVER, appId, masterKey, API_SAMPLE_RESULT_TEMPLATE } from "../Config/index"
import Credentials from '../Services/Credentials';
import ModalLoading from './ModalLoading';
import InstagramLogin from 'react-native-instagram-login'
import CookieManager from 'react-native-cookies';
Parse.initialize(appId, masterKey, masterKey);
Parse.serverURL = URL_SERVER;
Parse.setAsyncStorage(AsyncStorage);
// Parse.FacebookUtils.init({
//     appId      : '321327525449628', // Facebook App ID
//     status     : true,  // check Facebook Login status
//     cookie     : true,  // enable cookies to allow Parse to access the session
//     xfbml      : true,  // initialize Facebook social plugins on the page
//     version    : 'v2.3' // point to the latest Facebook Graph API version
//   });

var ParseComponent = ParseReact.Component(React);
class LoginScreen extends Component<Props> {
    state = {
        isLogin: true,
        email: '',
        password: '',
        loadingLogin: false
    }
    componentDidMount() {
        // CookieManager.clearAll()
        // .then((res) => {
        //   console.log('CookieManager.clearAll =>', res);
        //   this.setState({ token: '' })
        // });
        // AsyncStorage.clear();
        //this.props.login("ALO")
    }
    componentDidUpdate() {
        if (this.props.success) {
            console.log("sau khi log out")
            console.log(this.props.success);
            this.loginSuccess();
        } else if (this.props.error) {
            this.loginFailed(this.props.error);
        }
    }
    loginSuccess = () => {
        //Credentials.save(this.state.email, this.state.password);
        console.log("=============================sau khi log out")
        // Demo decides which page is auth root
        console.log("aaaaa", this.props.success)
        this.setState({
            loadingLogin: false
        }, () => this.props.navigation.replace('AllScreen'))

    }
    password = (text) => {
        this.setState({ password: text })
    }

    email = (text) => {
        this.setState({ email: text })
    }
    loginFailed = (error) => {
        this.props.resetError();
        let errorTitle = 'Invalid Login';
        let errorMsg = !!error ? error : 'If you forgot your password, use password recovery below.'

        Alert.alert(errorTitle,
            errorMsg,
            [

                {
                    text: 'Cancel',
                    onPress: () => this.setState({
                        loadingLogin: false
                    }),
                    style: 'cancel',
                },
                {
                    text: 'OK', onPress: () => {
                        this.setState({
                            loadingLogin: false
                        }), error == "If you forgot your password, use password recovery below." || error == "The password is incorrect" ? console.log("") : error == ""
                    }
                },
            ],
            { cancelable: false },
        );
            


    }
    clickLogin() {
        if (this.state.email == "" || this.state.password == "") {
            alert("Email or password wrong!")
        } else {
            let userLogin = {
                email: this.state.email,
                password: this.state.password
            }

            this.setState({
                loadingLogin: true
            })
            this.props.login(userLogin)
        }

    }
    alertLogin(message) {
        Alert.alert("Tài khoản hoặc mật khẩu không đúng",
            message,
            [

                {
                    text: 'Cancel',
                    onPress: () => this.setState({
                        loadingLogin: false
                    }),
                    style: 'cancel',
                },
                {
                    text: 'OK', onPress: () => {
                        this.setState({
                            loadingLogin: false
                        })
                    }
                },
            ],
            { cancelable: false },
        );

    }
    async loginUser(email, password) {
        console.log("email", email, "passpword", password)
        this.setState({
            loadingLogin: true
        })


        var user = new Parse.User;

        //console.log("user here", user)
        user.set("username", this.state.email);
        user.set("password", this.state.password)
        // const result = await user.logIn();
        user.logIn().then(async (user) => {
            console.log("user here", Parse.User.current().toJSON());
            let curUser = await Parse.User.current().toJSON();
            Credentials.saveToken(curUser.sessionToken);
            Credentials.saveUserId(curUser.objectId)
            this.props.navigation.replace('AllScreen')
            this.setState({
                loadingLogin: false
            })
        }).catch(function (error) {
            this.alertLogin("")
            console.log("Error: " + error.code + " " + error.message);
        }.bind(this));

    }
    async loginSocial(data) {
        console.log("data here ", data)
        Parse.User._logInWith('facebook', {
            authData: {
                id: data.user.id,
                access_token: data.credentials.access_token,
                expiration_date: data.credentials.data_access_expiration_time,

                //email:
            },
            email: data.user.email,
            fullname: data.user.first_name + " " + data.user.last_name
        }).then(async (result) => {
            let curUser = await Parse.User.current();

            if (!curUser.getEmail()) {

                curUser.setEmail(data.user.email)
                curUser.set("fullname", data.user.first_name + " " + data.user.last_name)
                curUser.save()
                console.log("login facebook here", curUser)
                let query = await new Parse.Query('UserProfile').equalTo("user", curUser).first();
                query.set("fullname", data.user.first_name + " " + data.user.last_name);
                query.set("avatar", data.user.picture.data.url);
                await query.save().then((value) => {
                    console.log("value here", value)

                }).catch(error => {

                    console.log("error", error)

                    // This will error, since the Parse.User is not authenticated
                });

            }
            let curNewUser = await Parse.User.current().toJSON();
            Credentials.saveToken(curNewUser.sessionToken);
            Credentials.saveUserId(curNewUser.objectId)
            this.props.navigation.navigate('AllScreen')
            this.setState({
                loadingLogin: false
            })



        }).catch(function (error) {
            console.log("login fail", error)
        });
    }
    test(token) {

        fetch('https://api.instagram.com/v1/users/self/?access_token=' + token)
            .then((response) => response.json())
            .then((responseJson) => {
                const resultData = responseJson.data;
                const id = resultData.id;
                let authData = {

                    id: id,
                    access_token: token

                };
                console.log("auth data", authData)
                var provider = {
                    authenticate(options) {
                        if (options.success) {
                            options.success(this, authData);
                        }
                    },
                    restoreAuthentication(authData) { },
                    getAuthType() {
                        return 'instagram';
                    },
                    deauthenticate() { }
                };
                //Parse.User._registerAuthenticationProvider(provider);
                const user = new Parse.User();
                user._linkWith("instagram", { authData }).then(async (result) => {
                    console.log("result here", result)
                    let curUser = await Parse.User.current();
                    curUser.set("fullname", resultData.full_name)
                    curUser.set("avatar", resultData.profile_picture)
                    let query = await new Parse.Query('UserProfile').equalTo("user", curUser).first();
                    query.set("fullname", resultData.full_name);
                    query.set("avatar", resultData.profile_picture);
                    await query.save().then((value) => {
                        console.log("value insta here", value)

                    }).catch(error => {

                        console.log("error", error)

                        // This will error, since the Parse.User is not authenticated
                    });
                    let curNewUser = await Parse.User.current().toJSON();
                    Credentials.saveToken(curNewUser.sessionToken);
                    Credentials.saveUserId(curNewUser.objectId)
                    this.props.navigation.navigate('AllScreen')
                }).catch(function (error) {
                    console.log("login fail", error)
                });
                // Parse.User._logInWith(provider, authData)
                // .then(async (result) => {
                //     console.log("result here",result)
                // }).catch(function (error) {
                //     console.log("login fail", error)
                // });
                console.log("result here", responseJson)

            })
            .catch((error) => {
                console.error(error);
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <InstagramLogin
                    ref={ref => this.instagramLogin = ref}
                    clientId='e6bfc1c7182e40699a7a4d38a151c29f'
                    redirectUrl='http://storii.vastbit.com'
                    scopes={['public_content+follower_list']}
                    onLoginSuccess={(token, data) => this.test(token)}
                    onLoginFailure={(data) => console.log(data)}
                />
                <ModalLoading visible={this.state.loadingLogin} />

                <View style={{ flex: 1 }}>
                    <View style={styles.header}>

                        {/* <Image style={styles.appLogo} resizeMode='contain' source={appLogo} /> */}
                        <SvgUri width="95" height="62" source={require('../../resource/logo.svg')} />

                        {/* <Text style={styles.appLogoSubTitle}>Join Your Team</Text> */}
                    </View>
                    <View style={styles.body}>
                        <KeyboardAvoidingView behavior="padding">
                            <View style={styles.emailInput}>
                                <TextInput style={styles.emailInputVal} keyboardType="email-address" autoCapitalize="none" onChangeText={this.email}
                                    placeholder="Email" placeholderTextColor={mainColorText} underlineColorAndroid="transparent" value={this.state.email} />
                                {/* <View style={styles.personIconContainer}>
                                    <Icon style={styles.iconStyle} active name='mail' />
                                </View> */}
                            </View>
                            <View style={styles.passwordInput}>
                                <TextInput style={styles.passwordInputVal} secureTextEntry={true} autoCapitalize="none" placeholder="Mật khẩu"
                                    onChangeText={this.password} placeholderTextColor={mainColorText} underlineColorAndroid="transparent" value={this.state.password} />
                                {/* <View style={styles.lockIconContainer}>
                                    <Icon style={styles.iconStyle} active name='lock' />
                                </View> */}
                                {/* <Image style={styles.visibilityIcon} source={visibilityIcon} />*/}
                            </View>
                            <View style={styles.forgotPasswordContainer}>
                                <TouchableHighlight onPress={() => {
                                    this.props.navigation.navigate("ForgotPasswordScreen")
                                }}><Text style={styles.forgotPasswordText}>Quên mật khẩu?</Text>
                                </TouchableHighlight>
                            </View>




                            {this.props.isLoading &&
                                <View style={{ zIndex: 3, marginTop: -45, marginBottom: 9 }}>
                                    <ActivityIndicator style={{}} animating={true} size='large' color='#231F20' />
                                </View>
                            }
                        </KeyboardAvoidingView>

                        <View style={[styles.registerContainer, { flex: 0.5, flexDirection: "row", width: 250 }]}>
                            <TouchableHighlight underlayColor='#999100' style={styles.loginButton} onPress={() => {
                                NetInfo.getConnectionInfo().then((connectionInfo) => {
                                    if (connectionInfo.type === "none") {
                                        Alert.alert('Kiểm tra lại kết nối internet');
                                    } else {
                                        if (this.state.password && this.state.email) {
                                            // this.props.postLoginUserInit(
                                            //     new LoginUser({ email: this.state.email, password: this.state.password }));
                                            this.loginUser(this.state.email, this.state.password)
                                            // Parse.User.logIn(this.state.email, this.state.password, {
                                            //     success: user => {
                                            //       console.log(user);
                                            //     },
                                            //     error: (user, error) => {
                                            //       console.log(user, error);
                                            //     },
                                            //   });
                                            //console.log("user here", user)
                                            //this.props.navigation.replace('AllScreen')
                                        } else {
                                            Alert.alert('Vui lòng điền Email và mật khẩu');
                                        }
                                    }
                                });

                            }}><Text style={styles.loginButtonText}>ĐĂNG NHẬP</Text>
                            </TouchableHighlight>

                            <TouchableHighlight underlayColor='#999100' style={[styles.loginButton, { marginLeft: "5%" }]} onPress={() => {
                                this.props.navigation.navigate("RegisterScreen")
                            }}><Text style={styles.loginButtonText}>ĐĂNG KÍ</Text>
                            </TouchableHighlight>
                            {/* <Button onPress={()=>this.click()}><Text>sadasd</Text></Button> */}
                        </View>
                        <ListItem style={{ borderBottomWidth: 0 }}>
                            <Text style={styles.orText}>hoặc</Text>
                            <View style={styles.socialMediaContainer}>

                                {/* <SocialIcon type='facebook' onPress={() => {
                               
                                //Alert.alert("Developing Feature");
                                facebook({
                                    appId: '321327525449628',
                                    callback: 'fb321327525449628://authorize',
                                   // scope: 'user_friends', // you can override the default scope here
  
                                    //scope: ['default'], // you can override the default scope here
                                    scope: ['email','user_birthday','user_gender','user_posts'], // you can override the default scope here
                                    fields: ['email', 'first_name', 'last_name','birthday','gender','age_range','picture'], // you can override the default fields here
                                }).then((info)=>{
                                    
                                    console.log("info facebook")
                                    console.log(info);
                                    this.loginSocial(info)
                                    // let data = {
                                    //     "type": SocialTypes.Facebook,
                                    //     "access_token": info.credentials.access_token,
                                    //     "email": info.user.email,
                                    //     "first_name": info.user.first_name,
                                    //     "last_name": info.user.last_name,
                                    //     "avatar": info.user.picture.data.url
                                    // }
                                    //console.log(data)
                                    // this.setState({
                                    //     infoSocial: data
                                    // })
                                    // this.props.loginSocial(data);
                                    // console.log(this.state.infoSocial)

                                    // if(info){

                                    // }

                                    //Alert.alert("Developing Feature");
                                }).catch((error) => {
                                    console.log("aaa",error)
                                    // error.code
                                    // error.description
                                });;
                                console.log("asdasd")
                                return;
                            }} /> */}
                                {/* <SocialIcon type='instagram' onPress={() => {
                                Alert.alert("Developing Feature", this.state.deviceToken);
                                return;
                            }} /> */}
                                <Button
                                    style={{ width: 50 }}
                                    onPress={() => {

                                        //Alert.alert("Developing Feature");
                                        facebook({
                                            appId: '321327525449628',
                                            callback: 'fb321327525449628://authorize',
                                            // scope: 'user_friends', // you can override the default scope here

                                            //scope: ['default'], // you can override the default scope here
                                            scope: ['email', 'user_birthday', 'user_gender', 'user_posts'], // you can override the default scope here
                                            fields: ['email', 'first_name', 'last_name', 'birthday', 'gender', 'age_range', 'picture'], // you can override the default fields here
                                        }).then((info) => {

                                            console.log("info facebook")
                                            console.log(info);
                                            this.loginSocial(info)
                                            // let data = {
                                            //     "type": SocialTypes.Facebook,
                                            //     "access_token": info.credentials.access_token,
                                            //     "email": info.user.email,
                                            //     "first_name": info.user.first_name,
                                            //     "last_name": info.user.last_name,
                                            //     "avatar": info.user.picture.data.url
                                            // }
                                            //console.log(data)
                                            // this.setState({
                                            //     infoSocial: data
                                            // })
                                            // this.props.loginSocial(data);
                                            // console.log(this.state.infoSocial)

                                            // if(info){

                                            // }

                                            //Alert.alert("Developing Feature");
                                        }).catch((error) => {
                                            console.log("aaa", error)
                                            // error.code
                                            // error.description
                                        });;
                                        console.log("asdasd")
                                        return;
                                    }}
                                    transparent>
                                    <SvgUri width="32" height="32" source={require('../../resource/facebook.svg')} />
                                    {/* <Image style={styles.imageSocialStyle} source={require("../../resource/facebook.png")} /> */}
                                </Button>
                                <Button onPress={() => this.instagramLogin.show()} style={{ width: 50 }} transparent>
                                    {/* <Image style={styles.imageSocialStyle} source={require("../../resource/instagram.png")} /> */}
                                    <SvgUri width="32" height="32" source={require('../../resource/instagram.svg')} />
                                </Button>

                            </View>
                        </ListItem>
                        {/* <Text style={styles.registerButtonNote}>Only accepting coach registrations</Text> */}
                    </View>
                    <View>
                        <Text style={styles.footerText}>By logging in you agree to STORII Activities Authority <Text onPress={() => { Linking.openURL('http://rosterspot.team/mobile/condition') }} style={styles.boldText}>Conditions</Text> of use and <Text onPress={() => { Linking.openURL('http://rosterspot.team/mobile/policy') }} style={styles.boldText}>Privacy Policy</Text></Text>
                    </View>
                </View>
            </View>
        );
    }
}
function mapStateToProps(state, props) {
    return {
        success: Selectors.LoginScreen.success(state),
        error: Selectors.LoginScreen.error(state),
        isLoading: Selectors.LoginScreen.isLoading(state),
    }
}
function mapDispatchToProps(dispatch) {
    return {
        login: (loginUser) => {
            dispatch(
                Actions.LoginScreen.login(loginUser)
            )
        },
        resetError: () => dispatch(Actions.LoginScreen.resetError()),


    }
}

//Connect everything
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: mainBackgroundColor

    },
    styleCenter: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    rowLogo: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowLogin: {
        flex: 0.3,
    },
    rowText: {
        flex: 0.05,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rowBottom: {
        flex: 0.3,
        justifyContent: 'center',
        marginTop: 30,



    },
    inputLoginStyle: {
        height: '60%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    formInput: {
        width: '90%', borderColor: mainColor, borderRadius: 10, height: 55
    },
    buttonLogin: {
        width: '90%',
        backgroundColor: mainColor,
        borderRadius: 10,
        height: 55,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonFacebook: {
        width: '90%',
        backgroundColor: "#3b5998",
        borderRadius: 10,
        height: 55,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        color: mainColorText,
        fontSize: normalize(18)
    },
    iconStyle: {
        color: mainColor,
        fontSize: 22
    },
    textHelp: {

        color: mainColor,
        textDecorationLine: 'underline',
        fontSize: normalize(14)


    },
    backgroundModal: {
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    drawLine: {
        width: "45%",
        borderTopWidth: 0.5,
        marginTop: 10
    },
    scrollView: {
        paddingVertical: 0,
        marginVertical: -40,
    },
    // container: {
    //     flex: 1,
    //     alignItems: 'stretch',
    //     // backgroundColor: '#231F20'
    // },
    keyboardAvoid: {
        flex: 1,
        alignItems: 'stretch',
    },
    header: {
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        //marginBottom: 25,
    },
    appLogo: {
        marginTop: 40,
        width: 95,
        height: 62,
    },
    appLogoSubTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'black',
        marginTop: -35,
    },
    body: {
        flex: 0.8,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    footerText: {
        color: mainColorText,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 13,
        lineHeight: 20
    },
    loginButton: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: "45%",
        backgroundColor: mainBackgroundColorButton,
        borderRadius: 5,
        //borderRightWidth: 1,
        //borderColor: '#999100',
    },
    loginButtonText: {
        alignItems: 'center',
        justifyContent: 'center',
        color: mainColorText,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: normalize(12),
    },
    registerButton: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: "50%",
        backgroundColor: mainBackgroundColorButton,
        borderTopRightRadius: 5,
        // borderBottomRightRadius: 5,
        // borderWidth: 0,
        // borderColor: '#999100',
    },
    registerButtonText: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#231F20',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: normalize(14),
    },
    registerButtonNote: {
        color: 'black',
        fontSize: 14,
        marginTop: -20,
        marginBottom: 25,
    },
    registerContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    boldText: {
        fontWeight: 'bold',
    },
    emailInput: {
        flexDirection: 'row-reverse',
    },
    emailInputVal: {
        backgroundColor: mainBackgroundColor,
        color: mainColorText,
        borderRadius: 2,
        borderBottomWidth: 1,
        borderColor: mainColorText,
        width: 250,
        minHeight: 50,
        paddingLeft: 10,
        marginBottom: 10,
        fontSize: normalize(13)
    },
    personIcon: {
        backgroundColor: mainColorText,
        height: 30,
        width: 30,
    },
    personIconContainer: {
        backgroundColor: mainColorText,
        minHeight: 50,
        minWidth: 50,
        borderRadius: 2,
        borderBottomWidth: 1,
        borderColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        top: 0,
    },
    passwordInput: {
        flexDirection: 'row-reverse',
    },
    passwordInputVal: {
        backgroundColor: mainBackgroundColor,
        color: mainColorText,
        borderRadius: 2,
        borderBottomWidth: 1,
        borderColor: mainColorText,
        width: 250,
        minHeight: 50,
        paddingLeft: 10,
        paddingRight: 45,
        marginBottom: 10,
        fontSize: normalize(13)
    },
    lockIcon: {
        backgroundColor: mainColorText,
        height: 35,
        width: 45,
        position: 'absolute',
        left: 0
    },
    lockIconContainer: {
        backgroundColor: mainColorText,
        minHeight: 50,
        minWidth: 50,
        borderRadius: 2,
        borderBottomWidth: 1,
        borderColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 0,
        top: 0,
    },
    visibilityIcon: {
        height: 21.5,
        width: 30.5,
        position: 'absolute',
        left: 10,
        top: 17.5,
    },
    forgotPasswordContainer: {
        height: 20,
        width: 250,
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        marginBottom: 25,
    },
    forgotPasswordText: {
        color: mainColorText,
        fontSize: normalize(13)
    },
    orText: {
        color: mainColorText,
        fontSize: 18,
        // marginTop: 10,
        marginBottom: 40,

    },
    socialMediaContainer: {
        width: 200,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    socialButtonImg: {
        height: 70,
        width: 70,
        margin: 5,
    },
    loadingStyle: {


        alignItems: 'center',
        flex: 1
    },
    notification: {
        width: '95%',
        height: 100,
        backgroundColor: 'rgba(236, 240, 241, 0.9)',

        borderRadius: 10,

    },

    buttonNotification: {
        width: '100%',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 17,
        borderRadius: 10
    },
    appNameStyle: {
        marginLeft: 17,
        fontSize: 16
    },
    titleNotification: {
        fontWeight: 'bold',
        marginLeft: 17,
    },
    bodyNotification: {
        marginLeft: 17,
    },
    imageSocialStyle: {
        width: 32,
        height: 32
    }

});
