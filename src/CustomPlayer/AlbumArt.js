import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';
import Swiper from 'react-native-swiper';
const AlbumArt = ({
  url,
  onPress,
  lyrics
}) => (
    <View style={styles.container}>
      <Swiper
      //showsPagination={false}
      dotColor={"white"}
      style={styles.wrapper} 
      //showsButtons={true}
      >
        <ScrollView>
          <Text style={{ fontWeight: 'bold', textAlign: "center",color:'white' }}>Lời bài hát</Text>
          <Text style={{ color: 'white', textAlign: "center" ,marginBottom:40}}>{lyrics}</Text>
        </ScrollView>
        {/* <TouchableOpacity style={{justifyContent:'center',alignItems:'center'}} onPress={onPress}>
          <Image
            style={styles.image}
            source={{ uri: url }}
          />
        </TouchableOpacity> */}

      </Swiper>
    </View>
  );

export default AlbumArt;

const { width, height } = Dimensions.get('window');
const imageSize = width - 100;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 24,
    paddingRight: 24,
    height: "45%",
  },
  image: {
    width: imageSize,
    height: imageSize,
  },
})
