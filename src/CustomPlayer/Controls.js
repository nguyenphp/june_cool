import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'native-base'
import { mainBackgroundColorButton } from '../Style/CustomStyle';
const Controls = ({
  paused,
  shuffleOn,
  repeatOn,
  onPressPlay,
  onPressPause,
  onBack,
  onForward,
  onPressShuffle,
  onPressRepeat,
  forwardDisabled,
}) => (
  <View style={styles.container}>
    <TouchableOpacity activeOpacity={0.0} onPress={onPressShuffle}>
      <Image style={[styles.secondaryControl, shuffleOn ? [] : styles.off]}
        source={require('../img/ic_shuffle_white.png')}/>
    </TouchableOpacity>
    <View style={{width: 40}} />
    <TouchableOpacity style={styles.nextButton} onPress={onBack}>
      <Icon style={{fontSize:18}} name="skip-backward"/>
      {/* <Image source={require('../img/ic_skip_previous_white_36pt.png')}/> */}
    </TouchableOpacity>
    <View style={{width: 20}} />
    {!paused ?
      <TouchableOpacity style={styles.playButton} onPress={onPressPause}>
        {/* <View style={styles.playButton}> */}
        <Icon style={{fontSize:24}} name="pause"/>
          {/* <Image source={require('../img/ic_pause_white_48pt.png')}/> */}
        {/* </View> */}
      </TouchableOpacity> :
      <TouchableOpacity style={styles.playButton} onPress={onPressPlay}>
        {/* <View style={styles.playButton}> */}
           <Icon style={{fontSize:24}} name="play"/>
          {/* <Image source={require('../img/ic_play_arrow_white_48pt.png')}/> */}
        {/* </View> */}
      </TouchableOpacity>
    }
    <View style={{width: 20}} />
    <TouchableOpacity style={styles.nextButton}  onPress={onForward}
      disabled={forwardDisabled}>
        <Icon style={{fontSize:18}} name="skip-forward"/>
      {/* <Image style={[forwardDisabled && {opacity: 0.3}]}
        source={require('../img/ic_skip_next_white_36pt.png')}/> */}
    </TouchableOpacity>
    <View style={{width: 40}} />
    <TouchableOpacity activeOpacity={0.0} onPress={onPressRepeat}>
      <Image style={[styles.secondaryControl, repeatOn ? [] : styles.off]}
        source={require('../img/ic_repeat_white.png')}/>
    </TouchableOpacity>
  </View>
);

export default Controls;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 8,
  },
  playButton: {
    height: 38,
    width: 38,
    //borderWidth: 1,
   // borderColor: 'white',
    borderRadius: 38 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:mainBackgroundColorButton
  },
  nextButton: {
    height: 24,
    width: 24,
    //borderWidth: 1,
   // borderColor: 'white',
    borderRadius: 24 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:mainBackgroundColorButton
  },
  secondaryControl: {
    height: 18,
    width: 18,
  },
  off: {
    opacity: 0.30,
  }
})
