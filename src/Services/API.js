
// import Axios from "axios";



// export default class API {
//     static async getAllNotification(page: number) {
//         const uri = API_GET_ALL_NOTIFICATION + "?page=" + page+"&limit=15";
//         const res = Axios.get(
//             uri
//         )

//         return res
//     }
// }

import axios from 'axios';
const POST = "POST";
const GET = "GET";
const DELETE = "DELETE";
const domain = "http://nongsanogreen.com/nhstory/public/";
const end_point = "api/"
const main_api = domain + end_point
const token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbm9uZ3Nhbm9ncmVlbi5jb20vbmhzdG9yeS9wdWJsaWMvYXBpL2xvZ2luIiwiaWF0IjoxNjAwMTU2NDEwLCJuYmYiOjE2MDAxNTY0MTAsImp0aSI6InBiQmF0U0ExdW9EZFdtUkkiLCJzdWIiOjMsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.2UQJTUhaHOEfvaykMHiLIf6tEtz61tzkIvWRzMpwI3Q";
export default class API {
    
    static async request(url, data) {
        let startTime = Date.now();
        let logData =
            '<============start==============' +
            '\n' +
            `URL: ${url}` +
            '\n' +
            `BODY: ${data.body}`;
        try {
            let response;
            if (data.method == GET) {
                response = await axios.get(url, data.body,{headers: data.headers});
            } else if (data.method == POST) {
                response = await axios.post(url, data, {headers: data.headers});
            } else if (data.method == DELETE) {
                response = await axios.delete(url, {headers: data.headers});
            }
            logData += '\n' + `RESPONSE STATUS: ${response.status}`;
            const json = response.data;
            logData += '\n' + `JSON RESPONSE:` + JSON.stringify(json);
            //console.log(logData + '\n' + '===============end============>');
            console.log('---- timed: ', Date.now() - startTime);
            return json;
        } catch (err) {
            logData += '\n' + `ERROR: ${err}`;
            console.log(logData + '\n' + '===============end============>');
        }
    }
    static async getStory() {
        const uri = main_api + "stories"
        const data = {
            method: GET,
            headers: {
                'Content-Type': 'application/json',
                // Authentication: token
            },
            // body: JSON.stringify({id})
        };
        return await this.request(uri, data);
    }
    static async getListMusic() {
        const uri = main_api + "songs";
        const data = {
            method: GET,
            headers: {
                'Content-Type': 'application/json',

                // Authentication: token
            },
            // body: JSON.stringify({id})
        };
        return await this.request(uri, data);
    }
    static async getListPhoto(page) {
        const uri = main_api + "photos?page="+page+"&size=20";
        const data = {
            method: GET,
            headers: {
                'Content-Type': 'application/json',
                //"Authentication": token
            },
            // body: JSON.stringify({id})
        };
        return await this.request(uri, data);
    }
    static async getAvatar() {
        const uri =  main_api + "profiles/1";
        const data = {
            method: GET,
            headers: {
                'Content-Type': 'application/json',
                //"Authentication": token
                // Authentication: token
            },
            // body: JSON.stringify({id})
        };
        return await this.request(uri, data);
    }
    static async login() {
        const uri = "http://nongsanogreen.com/nhstory/public/api/login";
        let formData = new FormData();
        formData.append("email","nguyenphp305@gmail.com")
        formData.append("password","conchosoi113");
        const data = {
            method: POST,
            headers: {
                'Content-Type': 'application/json',
                // Authentication: token
            },
            formData
        };
        return await this.request(uri, data);
    }
}

