import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 375;

export function normalize(size) {
  const newSize = size * scale
  if (Platform.OS === 'ios') {
    if (PixelRatio.get() === 2) {
      return Math.round(PixelRatio.roundToNearestPixel(newSize*1.27))
    } else if (PixelRatio.get() === 3) {
      if (SCREEN_WIDTH == 414) {
        return Math.round(PixelRatio.roundToNearestPixel(newSize * 1.27))
      } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize*1.27))
      }

    }

  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}
export function resizeImageHeight() {
  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width * 9 / 16);
  const cardHeight = Math.round(imageHeight / 2.2);
  const imageWidth = dimensions.width;
  
  if (PixelRatio.get() === 2) {
    return imageHeight*0.8
  } else if (PixelRatio.get() === 3) {
    if (SCREEN_WIDTH == 414) {
      return Math.round(imageHeight*0.97)
    } else {
      return imageHeight*1.009
    }

  }

}
export function resizeImageHeightOfSound() {
  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width * 9 / 16);
  const cardHeight = Math.round(imageHeight / 1.46);
  const imageWidth = dimensions.width;
  if (PixelRatio.get() === 2) {
    return imageHeight*1.274*(2/3)
  } else if (PixelRatio.get() === 3) {
    if (SCREEN_WIDTH == 414) {
      return Math.round(imageHeight*1.34*(2/3))
    } else {
      return imageHeight*1.479*(2/3)
    }

  }

}
export function resizeAvatar() {
  // const dimensions = Dimensions.get('window');
  // const imageHeight = Math.round(dimensions.width * 9 / 16);
  // const cardHeight = Math.round(imageHeight / 2.2);
  // const imageWidth = dimensions.width;
  if (PixelRatio.get() === 2) {
    return 80
  } else if (PixelRatio.get() === 3) {
    if (SCREEN_WIDTH == 414) {
      return 100
    } else {
      return 100
    }

  }

}
export function marginTopAvatar() {
  // const dimensions = Dimensions.get('window');
  // const imageHeight = Math.round(dimensions.width * 9 / 16);
  // const cardHeight = Math.round(imageHeight / 2.2);
  if (PixelRatio.get() === 2) {
    return 15
  } else if (PixelRatio.get() === 3) {
    if (SCREEN_WIDTH == 414) {
      return 30
    } else {
      return 30
    }

  }

}