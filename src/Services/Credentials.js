import {
    AsyncStorage,
} from 'react-native';
import Actions from '../Actions';
import Constants from '../Constants';

async function saveToken(token) {
    try {
        await AsyncStorage.setItem(Constants.Persist.Credentials.TOKEN, token);
    } catch (e) {
        console.error('Failed to save credentials.')
    }
}

async function loadToken() {
    try {
        const token = await AsyncStorage.getItem(Constants.Persist.Credentials.TOKEN);
        return token;
    } catch (e) {
        return "";
    }
}
async function saveUserId(userId) {
  try {
      await AsyncStorage.setItem(Constants.Persist.Credentials.USER_ID, userId);
  } catch (e) {
      console.error('Failed to save credentials.')
  }
}

async function loadUserId() {
  try {
      const userId = await AsyncStorage.getItem(Constants.Persist.Credentials.USER_ID);
      return userId;
  } catch (e) {
      return "";
  }
}
async function saveProfile(profile) {
    try {
      await AsyncStorage.setItem(Constants.Persist.Credentials.PROFILE, JSON.stringify(profile));
    } catch (e) {
      console.error('Failed to save profile.')
    }
  }
  async function loadProfile() { 
    try {
     const profile = await AsyncStorage.getItem(Constants.Persist.Credentials.PROFILE);
     return JSON.parse(profile);
    } catch (e) {
      return {};
    }
  }
  async function isFirstTimeLoad() {
    try {
      const token = await AsyncStorage.getItem(Constants.Persist.Credentials.FIRST_TIME_LOAD);
      if(!token) {
        await AsyncStorage.setItem(Constants.Persist.Credentials.FIRST_TIME_LOAD, "true");
        return true;
      }
      return false;
    } catch (e) {
      console.error('Failed to save credentials.')
    }
  }
async function logOut() {
    try {
    //   await AsyncStorage.removeItem(Constants.Persist.Credentials.ASYNC_EMAIL_STORAGE_KEY);
    //   await AsyncStorage.removeItem(Constants.Persist.Credentials.ASYNC_PASSWORD_STORAGE_KEY);
      await AsyncStorage.removeItem(Constants.Persist.Credentials.TOKEN);
      //await AsyncStorage.removeItem(Constants.Persist.Credentials.PROFILE);
      //await AsyncStorage.removeItem(Constants.Persist.Credentials.DEVICE_TOKEN);
    } catch (e) {
      console.error('Failed to save credentials.')
    }
  }
async function saveLanguage(lang){
  try {
    await AsyncStorage.setItem(Constants.Persist.Credentials.LANGUAGE, lang);
  } catch (e) {
    console.error('Failed to save lang.')
  }
}
async function loadLanguage() { 
  try {
   const lang = await AsyncStorage.getItem(Constants.Persist.Credentials.LANGUAGE);
   return lang
  } catch (e) {
    return {};
  }
}
async function saveDeviceToken(deviceToken) {
  try {
      await AsyncStorage.setItem(Constants.Persist.Credentials.DEVICE_TOKEN, deviceToken);
  } catch (e) {
      console.error('Failed to save credentials.')
  }
}

async function loadDeviceToken() {
  try {
      const deviceToken = await AsyncStorage.getItem(Constants.Persist.Credentials.DEVICE_TOKEN);
      return deviceToken;
  } catch (e) {
      return "";
  }
}
export default {
    // load,
    // save,
    saveToken,
    loadToken,
    logOut,
    isFirstTimeLoad,
    saveProfile,
    loadProfile,
    saveLanguage,
    loadLanguage,
    saveUserId,
    loadUserId,
    // deletePassword,
    // saveId,
    // loadId,
    saveDeviceToken,
    loadDeviceToken,
    // isSocial,
    //loadIsSocial
};