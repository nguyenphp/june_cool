export const success = (state) => state.LoginScreen.success;
export const error = (state) => state.LoginScreen.error;
export const isLoading = (state) => state.LoginScreen.isLoading;
export const getAllCountry = (state)=>state.LoginScreen.allCountry;
export const failMessage = (state) => state.LoginScreen.failMessage;
export const isCreatedSuccess = (state) => state.LoginScreen.isCreatedSuccess;