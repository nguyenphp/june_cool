/**
 * @author: Est <codeest.dev@gmail.com>
 * @date: 2017/5/19
 * @description:
 */

import {
    ADD_SONGS,
    POINT_SONG,
    NEXT_SONG,
    LAST_SONG,
    PAUSE,
    SEEK_PROGRESS,
    nextSong,
    progress,
    durationTime,
    SWITCH_MODE,
} from '../Actions/song'

//let Sound = require('react-native-sound');
import Sound from 'react-native-sound'
let storeInstance;

const playerMiddleware = store => next => action => {
    //console.log("aaaaa player")
    storeInstance = store;
    const thisState = store.getState();
    const result = next(action);
    const newState = store.getState();
    const actionType = String(action.type);
    if (newState === thisState) {
        return result;
    }
    switch (actionType) {
        case ADD_SONGS:
            if (!thisState.songs.isPlaying && newState.songs.isPlaying && newState.songs.currentSong !== null) {
                init(newState.songs.currentSong.audio_file);
            }
            break;
        case POINT_SONG:
            console.log("action song here",action.song)
            init(action.song.audio_file);
            break;
        case NEXT_SONG:
            if (newState.songs.isPlaying) {
                init(newState.songs.currentSong.audio_file);
            }
            break;
        case LAST_SONG:
            init(newState.songs.currentSong.audio_file);
            break;
        case PAUSE:
            play(newState.songs.isPlaying);
            switchMode(newState.songs.playMode)
            break;
        case SEEK_PROGRESS:
            seek(newState.songs.progressTime);
            break;
        case SWITCH_MODE:
            switchMode(newState.songs.playMode)
            break;
    }
    return result;
};

export default playerMiddleware;

let singletonSong = null;
let progressCountDown;

const init = (url) => {
    if (singletonSong !== null) {
        release();
    }
    console.log('url: '+ url);
    singletonSong = new Sound(url, '', (e) => {
        if (e) {
            console.log('failed to load the sound', e);
            return;
        }
        
        setTimeout(() => play(true), 200);
        getDurationTime();
        startProgress();
    });
};
const getDurationTime = () => {
    if (singletonSong !== null) {
        let durationTime1 = Math.round(singletonSong.getDuration())
        //console.log("durqtion time ",singletonSong.getDuration())
    storeInstance.dispatch(durationTime(durationTime1))
 
    }
    
}
const switchMode = (playMode) =>{
    console.log("current play mode", playMode)
    if(playMode=="loop"){
        singletonSong.setNumberOfLoops(-1);
    }else if(playMode=="one"){
        singletonSong.setNumberOfLoops(0);
    }
}
const play = (isPlaying) => {
   console.log("play here",isPlaying,singletonSong)
  
    if (!isPlaying) {
        singletonSong.pause();
        stopProgress();
    } else { 
        singletonSong.setCategory('Playback');
        singletonSong.play((success) => {
            if (success) {
                console.log('successfully finished playing');
                releaseAndNext();
            } else {
                init(singletonSong._filename)
                console.log('playback failed due to audio decoding errors');
            }
        });
        startProgress();
        //switchMode(this.thisState.songs.playMode);
    }
};

const release = () => {
    stopProgress();
    singletonSong.release();
};

const releaseAndNext = () => {
    release();
    storeInstance.dispatch(nextSong(true))
};

const seek = (time) => {
    singletonSong.setCurrentTime(time);
};

const startProgress = () => {
    if (singletonSong !== null) {
        progressCountDown = setInterval(() => {
            singletonSong.getCurrentTime((seconds) => storeInstance.dispatch(progress(seconds)));
        },1000);
    }
};

const stopProgress = () => {
    if (singletonSong !== null) {
        clearInterval(progressCountDown);
    }
};