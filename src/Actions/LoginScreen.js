import Constants from '../Constants';
export function addPost(test) {
    return {
        type: Constants.Actions.LoginScreen.LOGIN,
        test
    }
}
export function postLoginUserRequest() {
    return {
        type: Constants.Actions.LoginScreen.POST_LOGIN_USER_REQUEST,
    };
}
export function login(userLogin){
    return {
        type: Constants.Actions.LoginScreen.POST_LOGIN_USER_INIT,
        userLogin
    }
}
export function postLoginUserFail(error) {
    return {
        type: Constants.Actions.LoginScreen.POST_LOGIN_USER_FAIL,
        error
    };
}
export function postLoginUserSuccess(user) {
    return {
        type: Constants.Actions.LoginScreen.POST_LOGIN_USER_SUCCESS,
        user
    };
}
export function resetError() {
    return {
        type: Constants.Actions.LoginScreen.RESET_LOGIN_USER_ERROR,
    };
}
export function getProfileUser(data){
    return {
        type: Constants.Actions.LoginScreen.GET_PROFILE_USER,
        data
    };
}
export function registerAccount(userRegister){
    return {
        type: Constants.Actions.LoginScreen.POST_REGISTER_ACCOUNT,
        userRegister
    };
}
export function getAllCountry(){
    return {
        type: Constants.Actions.LoginScreen.GET_ALL_COUNTRY,
    };
}
export function setAllCountry(allCountry){
    return{
        type: Constants.Actions.LoginScreen.SET_ALL_COUNTRY,
        allCountry
    }
}
export function createNewAccountFailed(data) {
    return {
        type: Constants.Actions.LoginScreen.CREATE_ACCOUNT_FAILED,
        failMessage: data.message,
    };
}
export function createNewAccountSuccess(data) {
    return {
        type: Constants.Actions.LoginScreen.CREATE_ACCOUNT_SUCCESS,
        isCreatedSuccess: true,
    };
}
export function setFailMessage(message) {
    return {
        type: Constants.Actions.LoginScreen.CREATE_ACCOUNT_MESSAGE,
        failMessage: message,
    };
}
export function updateCreatedSuccess(status) {
    return {
        type: Constants.Actions.LoginScreen.CREATE_ACCOUNT_SUCCESS_STATUS,
        isCreatedSuccess: status,
    };
}
export function logOut(){
    return {
        type: Constants.Actions.LoginScreen.POST_LOGOUT_USER_SUCCESS,
    }
}