export const ADD_SONG = 'ADD_SONG';       
export const ADD_SONGS = 'ADD_SONGS';     
export const POINT_SONG = 'POINT_SONG';  
export const DELETE_SONG = 'DELETE_SONG';  
export const CLEAR_SONGS = 'CLEAR_SONGS';  
export const NEXT_SONG = 'NEXT_SONG';      
export const LAST_SONG = 'LAST_SONG';   
export const PAUSE = 'PAUSE';              
export const SWITCH_MODE = 'SWITCH_MODE';  
export const PROGRESS = 'PROGRESS';                  
export const SEEK_PROGRESS = 'SEEK_PROGRESS';         
export const LOVE = 'LOVE';           
export const LOCAL = 'LOCAL';        
export const DURATION_TIME = "DURATION_TIME";
export const addSong = () => {
    return {
        type: ADD_SONG,
    };
};

export const addSongs = (songs) => {
    return {
        type: ADD_SONGS,
        songs: songs
    };
};

export const pointSong = (song) => {
    return {
        type: POINT_SONG,
        song: song
    };
};

export const deleteSong = (index) => {
    return {
        type: DELETE_SONG,
        index: index
    };
};

export const clearSong = () => {
    return {
        type: CLEAR_SONGS,
    };
};

export function nextSong(isFinish) {
    return {
        type: NEXT_SONG,
        isFinish: isFinish
    };
}

export function lastSong(isFinish) {
    return {
        type: LAST_SONG,
        isFinish: isFinish
    };
}

export const pause = () => {
    return {
        type: PAUSE
    };
};

export const switchMode = () => {
    return {
        type: SWITCH_MODE
    };
};

export const progress = (time) => {
    return {
        type: PROGRESS,
        time: time
    };
};
export const durationTime = (durationTime) => {
    return {
        type: DURATION_TIME,
        durationTime: durationTime
    };
};
export const seekProgress = (time) => {
    return {
        type: SEEK_PROGRESS,
        time: time
    };
};

export const love = (song) => {
    return {
        type: LOVE,
        song: song
    };
};

export const local = (song) => {
    return {
        type: LOCAL,
        song: song
    };
};