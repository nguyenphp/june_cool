export class UserProfile {
    id;
    fullName;
    email;
    dateOfBirth;
    address;
    nationality;
    phoneNumber;

    constructor({
            id = -1,
            fullName = "",
            email = "",
            dateOfBirth = "",
            address = "",
            nationality = "",
            phoneNumber = ""
        }= {}) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.nationality = nationality;
        this.phoneNumber = phoneNumber;

        
    }
}