export class CommentInfo {
    objectId;
    content;
    fullname;
    userId;
    avatar;

    constructor({
        objectId = -1,
        content = "",
        fullname = "",
        userId = -1,
        avatar = ""

    } = {}) {
        this.objectId = objectId;
        this.fullname = fullname;
        this.content = content;
        this.userId = userId;
        this.avatar = avatar;


    }
}