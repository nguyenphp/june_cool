export class CreatorProfile {
    id;
    fullname;
    dob;
    address;
    phoneNumber;
    avatar;
    coverImage;
    aliasName;
    isSubscribed;
    subscriberCount;
    mediaContentCount;

    constructor({
            id = "",
            fullname = "",
            dob = "",
            address = "",
            phoneNumber = "",
            avatar = "",
            coverImage = "",
            aliasName = "",
            isSubscribed = false,
            subscriberCount = 0,
            mediaContentCount = 0


        }= {}) {
            this.id = id,
            this.fullname = fullname,
            this.dob = dob,
            this.address = address,
            this.phoneNumber = phoneNumber,
            this.avatar = avatar,
            this.coverImage = coverImage,
            this.aliasName = aliasName,
            this.isSubscribed = isSubscribed,
            this.subscriberCount = subscriberCount,
            this.mediaContentCount = mediaContentCount

        
    }
}