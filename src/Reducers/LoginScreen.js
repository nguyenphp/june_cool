import Constants from '../Constants';
const initialState = {
    test: "null",
    isLoading: false,
    success: false,
    error: null,

};
export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.Actions.LoginScreen.LOGIN:
            return {
                ...state,
                test: action.test
            }
        case Constants.Actions.LoginScreen.POST_LOGIN_USER_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        // Failed Login
        case Constants.Actions.LoginScreen.POST_LOGIN_USER_FAIL:
            return {
                ...state,
                error: action.error,
                isLoading: false
            }
        // Success Login
        case Constants.Actions.LoginScreen.POST_LOGIN_USER_SUCCESS:
            return {
                ...state,
                success: true,
                isLoading: false,
                account: action.user
            }
        case Constants.Actions.LoginScreen.RESET_LOGIN_USER_ERROR:
            return {
                ...state,
                error: null,
            }
        case Constants.Actions.LoginScreen.SET_ALL_COUNTRY:
            return {
                ...state,
                allCountry: action.allCountry,
            }
        case Constants.Actions.LoginScreen.POST_LOGOUT_USER_SUCCESS:
            return {
                ...state,
                success: false,
                isLoading: false,
                account: action.user
            }
        case Constants.Actions.LoginScreen.CREATE_ACCOUNT_SUCCESS_STATUS:
            return {
                ...state,
                isCreatedSuccess: action.isCreatedSuccess,
            }
        case Constants.Actions.LoginScreen.CREATE_ACCOUNT_SUCCESS:
            return {
                ...state,
                isCreatedSuccess: action.isCreatedSuccess,
            }
        case Constants.Actions.LoginScreen.CREATE_ACCOUNT_FAILED:
            return {
                ...state,
                failMessage: action.failMessage,
            }

        case Constants.Actions.LoginScreen.CREATE_ACCOUNT_MESSAGE:
            return {
                ...state,
                failMessage: action.failMessage,
            }
        default:
            return state;
    }
}