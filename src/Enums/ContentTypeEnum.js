

export default ContentTypeEnum = {
    video: 1,
    music: 2,
    musicAlbum: 3
}
