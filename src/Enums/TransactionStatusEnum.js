

export default TransactionStatusEnum = {
    OutOfMoney: 101,
    Fail: 500,
    Success: 200,
}