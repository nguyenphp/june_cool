

export default PaymentTypeEnum = {
    CreditCard: 1,
    Momo: 2,
    ZaloPay: 3
}
