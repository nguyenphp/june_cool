
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import BottomTab from './src/navigation/TabBarNavigation';
import { Provider, connect } from 'react-redux';
import configureStore from './src/Stores';
import RootNavigation from './src/navigation/RootNavigation';
import FlashMessage from "react-native-flash-message";
import Axios from 'axios';
import ModalLoading from './src/components/ModalLoading';
const datastore = configureStore({});
export const TRACKS = [
  {
    title: 'Hạnh ơi...',
    artist: 'A Chủn ft. A Củn',
    albumArtUrl: require('./resource/hanhoi1.png'),
    audioUrl: "https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3",
  },
  {
    title: 'Love Yourself',
    artist: 'Justin Bieber',
    albumArtUrl: "http://arrestedmotion.com/wp-content/uploads/2015/10/JB_Purpose-digital-deluxe-album-cover_lr.jpg",
    audioUrl: 'http://srv2.dnupload.com/Music/Album/Justin%20Bieber%20-%20Purpose%20(Deluxe%20Version)%20(320)/Justin%20Bieber%20-%20Purpose%20(Deluxe%20Version)%20128/05%20Love%20Yourself.mp3',
  },
  {
    title: 'Hotline Bling',
    artist: 'Drake',
    albumArtUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/Drake_-_Hotline_Bling.png',
    audioUrl: 'http://dl2.shirazsong.org/dl/music/94-10/CD%201%20-%20Best%20of%202015%20-%20Top%20Downloads/03.%20Drake%20-%20Hotline%20Bling%20.mp3',
  },
];
export default function App() {
  const [loading,setLoading] = useState(false)
  // useEffect(()=>{
  //   setLoading(true)
  //   let config = {
  //     headers: {
  //         'Content-Type': 'application/json',

  //     },
  // };
  //   const uri = "http://nongsanogreen.com/nhstory/public/api/login";
  //   let formData = new FormData();
  //   formData.append("email","nguyenphp305@gmail.com")
  //   formData.append("password","conchosoi113");
  //   Axios.post(uri,formData,config).then((res)=>{
  //     setLoading(false)
  //     console.log("login here", res.data)
  //     Credentials.saveToken(res.data.token);
  //   }).catch((err)=>{
  //     setLoading(false)
  //    // alert("Lỗi rồi Chủn ơi, thử lại sau nè");
  //     console.log("err", err)});
  // },[])
  return (
    <>
       <StatusBar backgroundColor="white" barStyle="dark-content" />
    <Provider store={datastore}>

 
        {/* <Text>Open up App.js to start working on your app!</Text>
     
      <App/> */}
    <RootNavigation />
        <FlashMessage position="top" />
   

    </Provider>
</>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
